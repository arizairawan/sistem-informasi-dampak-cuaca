<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function slug($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text))
    {
        return 'n-a';
    }

    return $text;
}

function cekstatus($status, $tgl, $oleh)
{
    if($status == 'Y')
    {
        $tgl = date('d M Y', strtotime($tgl));
        return "<i class='fa fa-check text-success'></i> $tgl ($oleh)";        
    }
    else
    {
        return "<b class='text-warning'>Menunggu</b>";
    }
}
function cekstatusapprove($status, $tgl, $oleh)
{
    if($status == 'Y')
    {
        $tgl = date('d M Y', strtotime($tgl));
        return "<i class='fa fa-check text-success'></i> $tgl ($oleh)";        
    }
    else if($status == 'N')
    {
        $tgl = date('d M Y', strtotime($tgl));
        return "<i class='fa fa-ban text-danger'></i> $tgl ($oleh) <br> <span class='text-danger'>Di lokasi anda sudah memiliki relawan.</span>";        
    }
    else
    {
        return "<b class='text-warning'>Menunggu</b>";
    }
}



?>