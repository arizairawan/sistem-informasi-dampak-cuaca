<?php 
function simple_encrypt($string) 
{
    //$salt = '12345';
    //return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));

    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);
    return $output;
}

function simple_decrypt($string) 
{
    //$salt = '12345';
    //return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));

    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    return $output;
}

function encrypt_access($access,$kategori)
{
    if($kategori == 2)
    {
        $tenorm = array("b1", "b2", "b3", "c1");
    }
    else
    {
        $tenorm = array("a1","a2","b1", "b2", "b3", "b4", "b5","b6", "c1","c2","c3","d");
    }
    
    if (in_array($access, $tenorm)) {
        $output = "ok";
    }
    else
    {
        $output = "not";
    }
    return $output;
}

function nilaidanpangkat($nilai, $pangkat){
    if($pangkat == ''){
        $hasil = $nilai;
    }else{
        $hasil = $nilai." x 10<sup>".$pangkat."</sup>";
    }
    return $hasil;
}
function hitungpangkat($nilai, $pangkat){
    if($pangkat == ''){
        $hasil = $nilai;
    }else{
        $hasil = $nilai * pow(10,$pangkat);
        
    }
    return $hasil;
}
?>