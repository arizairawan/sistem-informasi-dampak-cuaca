<style>
    .info{
        margin-bottom: 20px;
        border: 1px solid #ccc;
        border-radius: 5px;
        padding: 5px;
    }
    .info a{
        color: #333
    }
    .info h4{
        font-weight: 600
    }
    .info img{
        height: 200px;
        width: 100%;
        object-fit: cover
    }
</style>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <h3>Informasi</h3>
            <div class="row" id="loadfoto"></div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>




<script>
    $(document).ready(function() {
        
        var url_data = "<?=base_url()?>index.php/informasi/loaddata";

        function loadfoto()
        {
            var gambar = "";
            $.ajax({
                type:'POST',
                url: url_data,
                dataType:"JSON",
                success: function (response) {
                    for(var i = 0; i < response.data.length; i++){
                        gambar += '<div class="col-sm-6 col-md-3"><div class="info"><a href="<?=base_url()?>publik/readinfo/'+response.data[i].id_informasi+'"><img style="width:100%" src="'+response.data[i].image_dir+'"><h4>'+response.data[i].judul_informasi+'</h4><i>'+response.data[i].tgl_update+'</i> </a></div></div>';
                    }
                    $('#loadfoto').html(gambar)
                }
            });
        }

        $(function(){
            loadfoto()
        });



    });
</script>