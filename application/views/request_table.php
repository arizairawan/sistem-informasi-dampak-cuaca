<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <?php if($_SESSION['level'] == 4) { ?>
                <div class="box-header">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModaladd">Tambah</button>
                </div>
                <?php } ?>
                
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama User</th>
                                <th>Data</th>
                                <th>Tgl Request</th>
                                <th>Verifikasi</th>
                                <th>Validasi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<div class="modal fade" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Tambah</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/request/addrequest" id="formadd" enctype="multipart/form-data">
<!--
                    <div class="form-group">
                        <label>Data</label>
                        <select name="data" class="form-control" required>
                            <option value="">-Data-</option>
                            <option value="curahhujan">Curah Hujan</option>
                            <option value="kelembapan">Kelembapan</option>
                            <option value="suhu">Suhu</option>
                        </select>
                    </div>
-->
                    <div class="form-group">
                        <label>Kota</label>
                        <select class="form-control" name="id_kota" id="kota" required>
                            <option value="all"> - Kota -</option>
                            <?php foreach($kota as $kotax): ?>
                            <option value="<?=$kotax['id_kota']?>" data-id="<?=$kotax['id_kota']?>"><?=$kotax['nama_kota']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <script>
                        $(document).on('change','#kota', function(){
                            var id_kota = $('#kota option:selected').attr('data-id');
                            $('#kec option.opsi').css('display','none');
                            $("#kec option[data-kota="+id_kota+"]").css('display','block');
                        });   
                    </script>
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <select class="form-control" name="id_kecamatan" id="kec">
                            <option value="all"> - Semua Kecamatan -</option>
                            <?php foreach($kecamatan as $kec): ?>
                            <option class="opsi" value="<?=$kec['id_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group row tgl_request_add">
                        <div class="col-sm-6">
                            <label>Tanggal Awal</label>
                            <input type="date" name="tgl_awal" class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Akhir</label>
                            <input type="date" name="tgl_akhir" class="form-control" required>
                        </div>
                    </div>
                    <script>
                        $(document).on('change', '.tgl_request_add input[name="tgl_awal"]', function(){
                            var tgl_awal = $(this).val();
                            $('.tgl_request_add input[name="tgl_akhir"]').attr('min', tgl_awal);
                        });
                    </script>
                    <div class="form-group">
                        <label>Tujuan Penggunaan</label>
                        <textarea class="form-control" name="tujuan_penggunaan" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Surat Permohonan</label>
                        <input type="file" name="surat_permohonan" class="form-control" required>
                    </div>


                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Edit</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/request/editrequest" id="formedit" enctype="multipart/form-data">
                    <input type="hidden" id="id_request" name="id_request">
<!--
                    <div class="form-group">
                        <label>Data</label>
                        <select name="data" id="data" class="form-control" required>
                            <option value="">-Data-</option>
                            <option value="curahhujan">Curah Hujan</option>
                            <option value="kelembapan">Kelembapan</option>
                            <option value="suhu">Suhu</option>
                        </select>
                    </div>
-->
                    <div class="form-group">
                        <label>Kota</label>
                        <select class="form-control" name="id_kota" id="kota2" required>
                            <option value="all"> - Kota -</option>
                            <?php foreach($kota as $kotax): ?>
                            <option value="<?=$kotax['id_kota']?>" data-id="<?=$kotax['id_kota']?>"><?=$kotax['nama_kota']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <script>
                        $(document).on('change','#kota2', function(){
                            var id_kota = $('#kota2 option:selected').attr('data-id');
                            $('#kec2 option.opsi').css('display','none');
                            $("#kec2 option[data-kota="+id_kota+"]").css('display','block');
                        });   
                    </script>
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <select class="form-control" name="id_kecamatan" id="kec2">
                            <option value="all"> - Semua Kecamatan -</option>
                            <?php foreach($kecamatan as $kec): ?>
                            <option class="opsi" value="<?=$kec['id_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group row tgl_request_edit">
                        <div class="col-sm-6">
                            <label>Tanggal Awal</label>
                            <input type="date" name="tgl_awal" id="tgl_awal" class="form-control" required>
                        </div>
                        <div class="col-sm-6">
                            <label>Tanggal Akhir</label>
                            <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control" required>
                        </div>
                    </div>
                    <script>
                        $(document).on('change', '.tgl_request_edit input[name="tgl_awal"]', function(){
                            var tgl_awal = $(this).val();
                            $('.tgl_request_edit input[name="tgl_akhir"]').attr('min', tgl_awal);
                        });
                    </script>
                    <div class="form-group">
                        <label>Tujuan Penggunaan</label>
                        <textarea class="form-control" id="tujuan" name="tujuan_penggunaan" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Surat Permohonan</label>
                        <input type="file" name="surat_permohonan" class="form-control">
                    </div>


                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        if(level == 4){
            var url_data = base_url + "index.php/request/loaddata/<?=simple_encrypt($_SESSION['id_user'])?>";
        }else{
            var url_data = base_url + "index.php/request/loaddata/";
        }
        
        var url_data2 = base_url + "index.php/request/loaddatabyid/";

        $("#example1").DataTable( {
            "ajax": url_data,
            "scrollX" : true,
            "columns": [
                { "data": "no" },
                { "data": "oleh" },
                { "data": "data" },
                { "data": "tgl_request" },
                { "data": "verifikasi" },
                { "data": "validasi" },
                {
                    "data": null,
                    "render": function (data) {
                        var linkdetail = '<a href="'+base_url+'request/detail/'+data.id_request+'" class="btn btn-primary btn-xs">Detail</a>'
                        if(level == 4 && data.status_verifikasi == 'N'){
                            return linkdetail + ' <button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModaledit" data-id="'+ data.id_request + '">Edit</button> <button class="btn btn-danger btn-xs hapus" data-id="'+ data.id_request + '">Hapus</button>';
                        }else if(level == 2 && data.status_verifikasi == 'N'){
                            return linkdetail + ' <button class="btn btn-success btn-xs verifikasi" data-id="'+ data.id_request + '">Verifikasi</button>';
                        }else if(level == 3 && data.status_verifikasi == 'Y'  && data.status_validasi == 'N'){
                            return linkdetail + ' <button class="btn btn-success btn-xs validasi" data-id="'+ data.id_request + '">Validasi</button>';
                        }else{
                            return linkdetail;
                        }
                    }
                }
            ]
        });


        $('#myModaledit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var id = button.data('id')

            $.ajax({
                type: 'POST',
                url: url_data2 + '/' + id,
                dataType: 'JSON',
                success: function(response) {
                    $("#data option[value='"+response.data+"']").attr('selected',true);
                    $("#kota2 option[value='"+response.id_kota+"']").attr('selected',true);
                    $("#kec2 option[value='"+response.id_kecamatan+"']").attr('selected',true);
                    $('#tgl_awal').val(response.tgl_awal);
                    $('#tgl_akhir').val(response.tgl_akhir);
                    $('#tgl_akhir').attr('min', response.tgl_awal);
                    $('#tujuan').val(response.tujuan_penggunaan);
                    $('#id_request').val(response.id_request);
                }
            });
        })

        $(document).on('submit','#formedit',function(e){
            e.preventDefault();
            var data = new FormData(this);

            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Diperbaharui",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaledit').modal('hide');
                            $("#formedit")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
            return false;
        });

        $(document).on('submit','#formadd',function(e){
            e.preventDefault();
            var data = new FormData(this);

            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Ditambahkan",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaladd').modal('hide');
                            $("#formadd")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
        });

        $(document).on('click','.hapus',function () {
            var form_data = {
                id: $(this).attr('data-id'),
            };
            swal({   
                title: "Hapus data",   
                text: "Apakah anda yakin akan menghapus data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, Hapus!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/request/hapus",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah dihapus",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    var table = $('#example1').DataTable();
                                    table.ajax.url(url_data).load();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal Dihapus!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses hapus data telah dibatakan", "error");   
                } 
            });
        });
        
        $(document).on('click','.verifikasi',function () {
            var form_data = {
                id: $(this).attr('data-id'),
            };
            swal({   
                title: "Verifikasi data",   
                text: "Apakah anda yakin akan memverifikasi data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, Verifikasi!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/request/verifikasi",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah diverifikasi",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal diverifikasi!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses verifikasi data telah dibatakan", "error");   
                } 
            });
        }); 

        $(document).on('click','.validasi',function () {
            var form_data = {
                id: $(this).attr('data-id'),
            };
            swal({   
                title: "validasi data",   
                text: "Apakah anda yakin akan memvalidasi data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, validasi!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/request/validasi",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah divalidasi",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal divalidasi!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses validasi data telah dibatakan", "error");   
                } 
            });
        }); 

    });
</script>







