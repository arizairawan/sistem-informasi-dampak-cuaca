
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <img src="<?=base_url()?>assets/foto/<?=$relawan['pas_foto']?>" style="max-height:200px;border:1px solid #ddd">
                            <h4>Nama relawan : <?=$relawan['nama_relawan']?> </h4>
                            <p><b>No KTP : </b><?=$relawan['no_ktp']?></p>
                            <p><b>Email : </b><?=$relawan['email']?></p>
                            <p><b>Telepon : </b><?=$relawan['telepon']?></p>
                            <p><b>Alamat : </b><?=$relawan['alamat']?></p>
                            <p><b>Kecamatan : </b><?=$kec['nama_kecamatan']?></p>
                            <p><b>Kota : </b><?=$kota['nama_kota']?></p>
                            <p><b>Tanggal Daftar : </b><?=$relawan['tgl_daftar']?></p>
                            <p><b>Status Data Pendaftaran : </b><?php echo $approvex; ?></p>
                            <p><?php echo $suspend; ?></p>
                        </div>
                        <div class="col-sm-6">
                            <b>KTP</b> <br>
                            <img src="<?=base_url()?>assets/ktp/<?=$relawan['foto_ktp']?>" style="width:100%;border:1px solid #ddd;padding:5px">
                            <hr>
                            <b>Hasil Pelatihan</b> <br>
                            <ul class="list-group">
                                <li class="list-group-item">File : <a target="_blank" href="<?=base_url()?>assets/hasil_pelatihan/<?=$hasil['hasil_pelatihan'];?>"><?=$hasil['hasil_pelatihan'];?></a></li>
                                <li class="list-group-item">Tgl Upload : <?=$hasil['tgl_upload'];?></li>
                                <li class="list-group-item">Verifikasi : <?php echo $verifikasix; ?> </li>
                                <li class="list-group-item">Validasi : <?php echo $validasix; ?> </li>
                            </ul>
                        </div>

                        <?php if($_SESSION['tipe'] == 'internal') { ?>
                        <div class="col-sm-12">
                            <hr>
                            <?php if($_SESSION['level'] == 2) { ?>
                            
                            
                            <button <?php if($relawan['approve_pendaftaran'] != 'B') { echo 'disabled'; } ?> id="approve" data-status="<?php if($relawan['approve_pendaftaran'] != 'Y') { echo 'Y'; } else { echo 'N'; } ?>" data-id="<?=simple_encrypt($relawan['id_relawan'])?>" class="btn btn-success"><?php if($relawan['approve_pendaftaran'] != 'Y') { echo 'Approve Pendaftaran'; } else { echo "Data Pendaftaran Telah diapprove"; } ?></button>
                            <button <?php if($relawan['approve_pendaftaran'] != 'B') { echo 'disabled'; } ?> id="approve" data-status="<?php if($relawan['approve_pendaftaran'] != 'N') { echo 'N'; } else { echo 'N'; } ?>" data-id="<?=simple_encrypt($relawan['id_relawan'])?>" class="btn btn-danger"><?php if($relawan['approve_pendaftaran'] != 'N') { echo 'Tolak Pendaftaran'; } else { echo "Data Pendaftaran Telah ditolak"; } ?></button>
                            
                            &nbsp;
                            
                            <button <?php if($hasil['status_validasi'] != 'N') { echo 'disabled'; } ?> id="verifikasi" data-status="<?php if($hasil['status_verifikasi'] == 'N') { echo 'Y'; } else { echo 'N'; } ?>" data-id="<?=simple_encrypt($relawan['id_relawan'])?>" class="btn btn-success"><?php if($hasil['status_verifikasi'] == 'N') { echo 'Verifikasi'; } else if($hasil['status_verifikasi'] == 'Y') { echo 'Batalkan Verifikasi'; } else{ echo "Menunggu Hasil Pelatihan"; } ?></button>
                            <?php }else if($_SESSION['level'] == 3){ ?>
                            <button <?php if($hasil['status_verifikasi'] == 'N' || $hasil['status_verifikasi'] == '') { echo 'disabled'; } ?> id="validasi" data-status="<?php if($hasil['status_validasi'] == 'N') { echo 'Y'; } else { echo 'N'; } ?>" data-id="<?=simple_encrypt($relawan['id_relawan'])?>" class="btn btn-success"><?php if($hasil['status_validasi'] == 'N' && $hasil['status_verifikasi'] != 'N') { echo 'validasi'; } else if($hasil['status_validasi'] == 'Y') { echo 'Batalkan validasi'; } else { echo 'Menunggu Verifikasi'; } ?></button>
                            <?php } ?>
                        </div>
                        <?php } else {  ?>
                        <div class="col-sm-12">
                            <hr>
                            <?php if( $relawan['approve_pendaftaran'] == 'Y' && ($hasil['status_verifikasi'] == 'N' || $hasil['status_verifikasi'] == '') ){?>
                            <a href="<?=base_url()?>relawan/editprofil" class="btn btn-primary">Edit Profil</a>
                            <button class="btn btn-primary"  data-toggle="modal" data-target="#myModalupload">Upload Hasil Pelatihan</button>
                            <?php } ?>
                            
                            <?php if($hasil['status_validasi'] == 'Y'){ ?>
                            <a target="_blank" href="<?=base_url()?>printsurat/sk/<?=simple_encrypt($relawan['id_relawan'])?>" class="btn btn-primary">Print SK</a>
                            <?php } ?>
                            
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="myModalupload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Hasil Pelatihan</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="formuploadhasil" action="<?=base_url()?>relawan/uploadhasilpelatihan" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>File Hasil Pelatihan</label>
                        <input type="file" name="hasil_pelatihan" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $(document).on('click','#verifikasi',function () {
            var form_data = {
                id: $(this).attr('data-id'),
                status: $(this).attr('data-status'),
            };
            swal({   
                title: "Verifikasi data",   
                text: "Apakah anda yakin akan memverifikasi data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, Verifikasi!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/relawan/verifikasi",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah diverifikasi",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal diverifikasi!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses verifikasi data telah dibatakan", "error");   
                } 
            });
        }); 

        $(document).on('click','#validasi',function () {
            var form_data = {
                id: $(this).attr('data-id'),
                status: $(this).attr('data-status'),
            };
            swal({   
                title: "validasi data",   
                text: "Apakah anda yakin akan memvalidasi data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, validasi!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/relawan/validasi",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah divalidasi",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal divalidasi!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses validasi data telah dibatakan", "error");   
                } 
            });
        }); 
        
        $(document).on('click','#aktifkan',function () {
            var form_data = {
                id: $(this).attr('data-id'),
            };
            swal({   
                title: "Aktivasi Akun",   
                text: "Apakah anda yakin akan mengaktifkan kembali akun ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, aktifkan!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/relawan/aktifkanakun",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah diaktifkan",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal diaktifkan!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses aktivasi data telah dibatakan", "error");   
                } 
            });
        }); 
        
        $(document).on('click','#approve',function () {
            var appr = $(this).attr('data-status');
            
            if(appr == 'Y'){
                var texttitle = "Approve";
            }else{
                var texttitle = "Tolak";
            }
            
            var form_data = {
                id: $(this).attr('data-id'),
                status: appr,
            };
            swal({   
                title: texttitle+ " data pendaftaran",   
                text: "Apakah anda yakin akan "+ texttitle +" data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, "+ texttitle +"!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/relawan/approve_pendaftaran",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah di"+ texttitle,
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal diapprove!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses approve data telah dibatakan", "error");   
                } 
            });
        }); 


        $(document).on('submit','#formuploadhasil',function(e){
            e.preventDefault();
            var data = new FormData(this);

            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });
            $.ajax({
                'type': 'POST',
                'url': $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Upload Hasil Pelatihan Berhasil",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            location.reload();
                        });
                    }
                    
                    else{
                        swal("Maaf!", "Upload Hasil Pelatihan Gagal!", "error")
                    }
                }
            });
        });
    });
</script>