<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="<?=base_url()?>assets/plugins/jQuery/jquery-3.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<div class="container">
    <br>
    <br>
    <br>
    <?=$this->session->flashdata('message');?>
    <form method="post" action="<?=base_url()?>importdata/prosesimportsuhu" enctype="multipart/form-data">
        <div class="form-group">
            <label>File Xls</label>
            <input type="file" name="file" accept=".xls" required class="form-control">
        </div>

        <div class="form-group">
            <label>Kota</label>
            <select class="form-control" name="id_kota" id="kota">
                <option value="all"> - Kota -</option>
                <?php foreach($kota as $kotax): ?>
                <option value="<?=$kotax['id_kota']?>" data-id="<?=$kotax['id_kota']?>"><?=$kotax['nama_kota']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <script>
            $(document).on('change','#kota', function(){
                var id_kota = $('#kota option:selected').attr('data-id');
                $('#kec option.opsi').css('display','none');
                $("#kec option[data-kota="+id_kota+"]").css('display','block');
            });   
        </script>
        <div class="form-group">
            <label>Kecamatan</label>
            <select class="form-control" name="id_kecamatan" id="kec">
                <option value="all"> - Kecamatan -</option>
                <?php foreach($kecamatan as $kec): ?>
                <option class="opsi" value="<?=$kec['id_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Import</button>
    </form>
</div>
