<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?=$title?> | SIDC</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/sweetalert.css">
        
        <link rel="stylesheet" href="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
page. However, you can choose any other skin. Make sure you
apply the skin class to the body tag so the changes take effect. -->
        <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/skins/skin-red.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    
        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 3.1.1 -->
        <script src="<?=base_url()?>assets/plugins/jQuery/jquery-3.1.1.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
        
        
        <script src="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

        <!-- CK Editor -->
        <script src="<?=base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>
        
        <!-- DataTables -->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        
        <script src="<?=base_url()?>assets/sweetalert.min.js"></script>

        

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

        
        <!-- AdminLTE App -->
        <script src="<?=base_url()?>assets/dist/js/adminlte.min.js"></script>
        <script>
            var base_url = "<?=base_url()?>";
            var level = "<?=$_SESSION['level']?>";
        </script>
        <style>
            table tr td{
                white-space: nowrap
            }
        </style>
    </head>
    <!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
    <body class="hold-transition skin-red sidebar-mini">
        <div class="wrapper">

            <!-- Main Header -->
            <header class="main-header">

                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>S</b>A</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Sistem</b>Cuaca</span>
                </a>
                
                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">


                            <!-- User Account Menu -->
                            <li class="dropdown user user-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#">
                                    <!-- The user image in the navbar-->
                                    <img src="<?=base_url()?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                    <span class="hidden-xs"><?=$_SESSION['nama']?></span>
                                </a>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="<?=base_url()?>login/out">Logout</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">


                    <!-- Sidebar Menu -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">HEADER</li>
                        <!-- Optionally, you can add icons to the links -->
                        <li <?php if($nav == 'home') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>home"><i class="fa fa-home"></i> <span>Home</span></a></li>
                        <li <?php if($nav == 'spasial') { echo 'class="active"'; } ?> ><a target="_blank" href="<?=base_url()?>publik/spasial"><i class="fa fa-map-marker"></i> <span>Spasial</span></a></li>
                        
                        <?php if($_SESSION['tipe'] == 'relawan') { ?>
                        <li <?php if($nav == 'relawan') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>relawan/detail/<?=simple_encrypt($_SESSION['id_user'])?>"><i class="fa fa-user"></i> <span>Profil</span></a></li>
                        <li <?php if($nav == 'lapor') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>lapor/"><i class="fa fa-cloud"></i> <span>Lapor</span></a></li>
                        <?php } ?>
                        
                        <?php if($_SESSION['tipe'] == 'stasiun') { ?>
<!--
                        <li <?php if($nav == 'laporstasiun') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>laporstasiun/"><i class="fa fa-cloud"></i> <span>Lapor Kelembapan</span></a></li>
                        <li <?php if($nav == 'laporstasiunsuhu') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>laporstasiun/suhu"><i class="fa fa-cloud"></i> <span>Lapor Suhu</span></a></li>
-->
                        <li <?php if($nav == 'laporstasiun') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>laporstasiun/"><i class="fa fa-cloud"></i> <span>Lapor</span></a></li>
                        <?php } ?>
                        
                        <?php if($_SESSION['tipe'] == 'internal' && $_SESSION['level'] != 4) { ?>
                        
                        
                        <li <?php if($nav == 'relawan') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>relawan"><i class="fa fa-users"></i> <span>Relawan</span></a></li>
                        <li <?php if($nav == 'stasiun') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>stasiun"><i class="fa fa-industry"></i> <span>Stasiun</span></a></li>
                        <li <?php if($nav == 'kategori') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>kategori"><i class="fa fa-list"></i> <span>Kategori Curah Hujan</span></a></li>
                        <li <?php if($nav == 'cuaca') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>cuaca"><i class="fa fa-cloud"></i> <span>Data Curah Hujan</span></a></li>
<!--
                        <li <?php if($nav == 'kelembapan') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>cuaca/kelembapan"><i class="fa fa-cloud"></i> <span>Data Kelembapan</span></a></li>
                        <li <?php if($nav == 'suhu') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>cuaca/suhu"><i class="fa fa-cloud"></i> <span>Data Suhu</span></a></li>
-->
                        
                        <?php if($_SESSION['level'] <= 2) { ?>
                        <li <?php if($nav == 'kota') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>kota"><i class="fa fa-cubes"></i> <span>Kota</span></a></li>
                        <li <?php if($nav == 'kecamatan') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>kecamatan"><i class="fa fa-cubes"></i> <span>Kecamatan</span></a></li>
                        <li <?php if($nav == 'informasi') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>informasi"><i class="fa fa-info-circle"></i> <span>Informasi</span></a></li>
                        <?php } ?>
                        
                        <?php if($_SESSION['level'] == 1) { ?>
                        <li <?php if($nav == 'user') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>user"><i class="fa fa-user"></i> <span>User</span></a></li>
                        <?php } ?>
                        
                        
                        
                        
                        <?php } ?>
                        
                        
                        <?php if($_SESSION['tipe'] == 'internal') { ?>
                        <li <?php if($nav == 'request') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>request"><i class="fa fa-send"></i> <span>Request Data</span></a></li>
                        <?php } ?>
                        
                        <?php if($_SESSION['level'] == 3) { ?>
                        <li <?php if($nav == 'laporan') { echo 'class="active"'; } ?> ><a href="<?=base_url()?>laporan"><i class="fa fa-bar-chart"></i> <span>Laporan</span></a></li>
                        <?php } ?>

                    </ul>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?=$title?>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content container-fluid">

                <?php $this->load->view($views); ?>
                    
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- Default to the left -->
                <strong>Copyright &copy; 2019 <a href="#">Sistem Informasi Dampak Cuaca</a>.</strong> All rights reserved.
            </footer>


        </div>
        <!-- ./wrapper -->

        
        
        <!-- Optionally, you can add Slimscroll and FastClick plugins.
Both of these plugins are recommended to enhance the
user experience. -->
    </body>
</html>