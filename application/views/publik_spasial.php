<link
      rel="stylesheet"
      href="https://js.arcgis.com/4.11/esri/themes/light/main.css"
    />
<script src="https://js.arcgis.com/4.11/"></script>

<style>
    html, body {
        padding: 0;
        margin: 0;
        height: 100%;
        width: 100%;
    }
    .container{
        width: 100%
    }
    #viewDiv {
        padding: 0;
        margin: 0;
        height: 80vh;
        width: 100%;
    }
</style>


<div class="container">
    <div class="row">
        <div class="col-sm-3" style="padding:10px">

            <div class="form-group">
                <label>Data</label>
                <select id="datanya" class="form-control">
                    <option value="relawan">Relawan</option>
                    <option value="stasiun">Stasiun</option>
                </select>
            </div>

            <div id="relawanview">
                <div class="form-group">
                    <label>Kota</label>
                    <div>
                        <select class="form-control" required name="id_kota" id="id_kota">
                            <option value="">- Kota -</option>
                            <?php foreach($kota as $ko): ?>
                            <option value="<?=$ko['id_kota']?>"><?=$ko['nama_kota']?></option>
                            <?php endforeach; ?>
                        </select> 
                    </div> 
                </div>
                <script>
                    $(document).on('change','#id_kota', function(){
                        var id_kota = $(this).val();
                        $('#id_kecamatan option.opsi').css('display','none');
                        $("#id_kecamatan option[data-kota="+id_kota+"]").css('display','block');
                        $("#id_kecamatan").val($("#id_kecamatan option:first").val());
                    });   
                </script>
                <div class="form-group">
                    <label>Kecamatan</label>
                    <div>
                        <select class="form-control" required name="id_kecamatan" id="id_kecamatan">
                            <option value="">- Kecamatan -</option>
                            <?php foreach($kecamatan as $kec): ?>
                            <option class="opsi" value="<?=$kec['id_spasial']?>" textnya="<?=$kec['nama_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                            <?php endforeach; ?>
                        </select> 
                    </div> 
                </div>
            </div>
            <div class="form-group">
                <button id="klik" class="btn btn-primary btn-block loadingnih" data-loading-text="Please Wait..." type="submit">Tampilkan</button>
            </div>

            <button id="hapussemua" class="btn btn-danger btn-block">Hapus Semua Layer</button>

            <div id="listlayer"></div>
        </div>
        <div class="col-sm-9">
            <div id="viewDiv"></div>
        </div>
    </div>
</div>
<div id="popupload">
    <button id="closex">x</button>
    <div class="resultDiv"></div>
</div>
<style>
    #popupload{
        position: fixed;
        top: 0;
        left: -300px;
        width: 300px;
        height: 100vh;
        background: #ffffff;
        transition: all 1s
    }
    #popupload.openx{
        left: 0;
    }
</style>


<script>
    var BASE_URL = "<?=base_url()?>";
    $(document).ready(function(){
        $(document).on('change','#datanya',function(){
            var datanya = $(this).val();
            if(datanya == 'relawan'){
                $('#relawanview').css('display','block')
            }else{
                $('#relawanview').css('display','none')
            }
        })  
    })

    require([

        "esri/tasks/Locator",

        "esri/Map",
        "dojo/dom-attr",
        "dojo/dom",
        "dojo/on",
        "dojo/dom-class",
        "dojo/query",
        "dojo/dom-construct",

        "esri/views/MapView",

        "dojo/request",
        "dojo/json",
        "dojo/_base/array",

        "esri/widgets/Legend",
        "esri/layers/FeatureLayer",
        
        "esri/widgets/LayerList",


        "dojo/domReady!"
    ],
            function(
            Locator,
             Map, domAttr, dom, on, domClass, query, domConstruct, MapView, request, JSON, arrayUtil, Legend,
             FeatureLayer, LayerList, Point,
            ) {



        var legend; 
        var map = new Map({
            basemap: "osm"
        });
        var view = new MapView({
            container: "viewDiv",  // Reference to the scene div created in step 5
            map: map,  // Reference to the map object created before the scene
            zoom: 11,  // Sets zoom level based on level of detail (LOD)    
            center: [106.834883,-6.220401], 
        });

        //url layer2 nya

        function zoomToLayer(layer) {
            return layer.queryExtent()
                .then(function(response) {
                view.goTo(response.extent);
            });
        }

        function addlegend(layer){
            
            if(legend != undefined){
                legend.destroy();
            }
            legend = new Legend({
                view: view,
                layerInfos: layer
            });

            // Add widget to the bottom right corner of the view
            view.ui.add(legend, "bottom-right");

        }


        //cek ada atau tidaknya layer
        var ruas;
        var statusnya; 
        var layernya;
        var legendtitle;

        //array

        var idstasiun = "710bb993bbf64c928468acd9686c36ac";
        var titlestasiun = "Stasiun";
        
        var arraylayerx = [];

        function cekarray(legendtitle)
        {
            var hasil = true;
            for(var x = 0; x < arraylayerx.length; x++){
                if(arraylayerx[x].title == legendtitle){
                    hasil = false;
                    console.log(arraylayerx[x].title +'=='+ legendtitle)
                }
            }
            
            return hasil;
        }
        
        function ceklayer(datanyanih, nama, kec)
        {   
            if(datanyanih == 'stasiun'){
                layernya = idstasiun;
                legendtitle = titlestasiun;
            }else{
                if(kec == 'undefined' || kec == ''){
                    layernya = 'no';
                    legendtitle = '';
                }else{
                    layernya = kec;
                    legendtitle = nama;
                }
            }
            
            if(layernya != 'no'){
                
                var cekarraydulu = cekarray(legendtitle);
                console.log(cekarraydulu)
                if(cekarraydulu == true){
                    var fl = new FeatureLayer({
                        portalItem: {  // autocasts as esri/portal/PortalItem
                            id: layernya
                        },
                        outFields: ["*"],
                        popupTemplate:{
                            title: legendtitle,
                            content: "No : {no} <br> Nama : {NAMA} <br> Koordinat : {x},{y} <br><a class='coba' href='"+BASE_URL+"publik/detail2/"+datanyanih+"/{no}' onclick='window.open(this.href, \"mywin\",\"left=20,top=20,width=1000,height=500,toolbar=1,resizable=0\"); return false;'>Detail</a> ",
                        }
                    });

                    map.add(fl);

                    var arrlayer = {
                        layer : fl,
                        title : legendtitle
                    }

                    arraylayerx.push(arrlayer);


                    addlegend(arraylayerx);
                }
                
                //                zoomToLayer(fl)
            }else{
                return 'no';
            }

        }
        //cek ada atau tidaknya layer

        //menampilkan layer by filter
        function getContent(){
            
            var kecamatan = dom.byId("id_kecamatan").value;
            var datanyanih = dom.byId("datanya").value;
            
            var x = document.getElementById("id_kecamatan").selectedIndex;
            var y = document.getElementById("id_kecamatan").options;
            var nama = y[x].text;
            
            
            var cek = ceklayer(datanyanih, nama, kecamatan);

            if(cek == 'no'){

                alert('layer tidak tersedia');

            }

        }

        on(dom.byId("klik"), "click", getContent);
        //menampilkan layer by filter

        on(dom.byId("hapussemua"), "click", function(){
            map.layers.removeAll();
//            legend.destroy();
            
            arraylayerx = [];
            addlegend(arraylayerx);
        });
        on(dom.byId("closex"), "click", function(){
            domClass.remove("popupload", "openx");
        });



    });


</script>
