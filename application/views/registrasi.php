<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Registrasi Relawan | Sistem Informasi Spasial Dampak Cuaca</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?=base_url()?>assets/plugins/select2/select2.min.css">
        
        <link rel="stylesheet" href="<?=base_url()?>assets/sweetalert.css">
        
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <style>
            .login-box{
                background: #ccc
            }
        </style>

        <!-- jQuery 3.1.1 -->
        <script src="<?=base_url()?>assets/plugins/jQuery/jquery-3.1.1.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>

        <script src="<?=base_url()?>assets/sweetalert.min.js"></script>
        
        <!-- Select2 -->
        <script src="<?=base_url()?>assets/plugins/select2/select2.full.min.js"></script>


    </head>
    <body class="hold-transition login-page" style="padding-top:40px">
        <div class="container login-box-body">
            <form action="<?=base_url()?>registrasi/proses" id="formregist" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Registrasi Relawan</h4>
                        <hr>
                    </div>

                    <div class="col-sm-6">                    
                        <div class="form-group row">
                            <label class="col-sm-4">Nama Lengkap</label>
                            <div class="col-sm-8">
                                <input type="text" name="nama_relawan" required class="form-control" placeholder="Nama Relawan">   
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">Scan KTP</label>
                            <div class="col-sm-8">
                                <input type="file" name="foto_ktp" required accept="image/*" class="form-control">   
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">No. KTP</label>
                            <div class="col-sm-8">
                                <input type="text" name="no_ktp" required class="form-control" placeholder="No KTP">   
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">Pas Foto</label>
                            <div class="col-sm-8">
                                <input type="file" name="pas_foto" required accept="image/*" class="form-control">   
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">Alamat</label>
                            <div class="col-sm-8">
                                <textarea name="alamat" required class="form-control" placeholder="Alamat"></textarea>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">Kota</label>
                            <div class="col-sm-8">
                                <select class="form-control" required name="id_kota" id="id_kota">
                                    <option value="">- Kota -</option>
                                    <?php foreach($kota as $ko): ?>
                                    <option value="<?=$ko['id_kota']?>"><?=$ko['nama_kota']?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div> 
                        </div>
                        <script>
                            $(document).on('change','#id_kota', function(){
                                var id_kota = $(this).val();
                                $('#id_kecamatan option.opsi').css('display','none');
                                $("#id_kecamatan option[data-kota="+id_kota+"]").css('display','block');
                            });   
                        </script>
                        <div class="form-group row">
                            <label class="col-sm-4">Kecamatan</label>
                            <div class="col-sm-8">
                                <select class="form-control" required name="id_kecamatan" id="id_kecamatan">
                                    <option value="">- Kecamatan -</option>
                                    <?php foreach($kecamatan as $kec): ?>
                                    <option class="opsi" value="<?=$kec['id_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div> 
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label class="col-sm-4">Telepon</label>
                            <div class="col-sm-8">
                                <input type="text" name="telepon" required class="form-control" placeholder="Telepon">   
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4">Email</label>
                            <div class="col-sm-8">
                                <input type="email" name="email" required class="form-control" placeholder="Email">   
                            </div> 
                        </div>
                        <div id="pass" class="form-group row">
                            <label class="col-sm-4">Password</label>
                            <div class="col-sm-8">
                                <input type="password" name="password1" required class="form-control" placeholder="Password">   
                            </div> 
                        </div>
                        <div id="pass2" class="form-group row">
                            <label class="col-sm-4">Konfirmasi password</label>
                            <div class="col-sm-8">
                                <input type="password" name="password2" required class="form-control" placeholder="Password">   
                            </div> 
                        </div>
                    </div>

                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-sm-offset-8 col-sm-2">
                        <a href="<?=base_url()?>login" class="btn btn-default btn-block btn-flat">Kembali</a>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary btn-block btn-flat klik">Submit</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>

        <script>
            
            $(document).on('submit','#formregist',function(e){
                e.preventDefault();
                var data = new FormData(this);
                
                $( document ).ajaxStart(function() {
                    $( ".klik" ).text('Loading...');
                }).ajaxStop(function() {
                    $( ".klik" ).text('Submit');
                });
                $.ajax({
                    'type': 'POST',
                    'url': $(this).attr('action'),
                    'data': data,
                    'processData': false,
                    'contentType': false,
                    'cache': false,
                    success: function(response) {
                        if(response == 'success'){
                            swal({
                                title: "Registrasi Berhasil",
                                type: "success",
                                confirmButtonText: "Okay",
                                closeOnConfirm: true
                            },
                                 function(){
                                location.href = "<?=base_url()?>login";
                            });
                        }
                        else if(response == 'password'){
                            swal("Maaf!", "Password tidak sesuai!", "error");
                            $('#pass input').val('');
                            $('#pass2 input').val('');
                            $('#pass').addClass('has-warning');
                            $('#pass2').addClass('has-warning');
                        }
                        else{
                            swal("Maaf!", "Registrasi Gagal!", "error")
                        }
                    }
                });
            });

        </script>
    </body>
</html>
