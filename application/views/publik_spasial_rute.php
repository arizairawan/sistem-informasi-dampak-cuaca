

        <link rel="stylesheet" href="https://js.arcgis.com/4.6/esri/css/main.css">
        <script src="https://js.arcgis.com/4.6/"></script>

        <style>
            html, body {
                padding: 0;
                margin: 0;
                height: 100%;
                width: 100%;
            }
            .container{
                width: 100%
            }
            #viewDiv {
                padding: 0;
                margin: 0;
                height: 100vh;
                width: 100%;
            }
        </style>


        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="viewDiv"></div>
                </div>
            </div>
        </div>


        <script>
            var BASE_URL = "<?=base_url()?>";
            
            require([

                "esri/tasks/Locator",

                "esri/Map",
                "dojo/dom",
                "dojo/on",
                "dojo/dom-class",

                "esri/views/MapView",


                "esri/Graphic",
                "esri/layers/GraphicsLayer",

                "esri/tasks/RouteTask",
                "esri/tasks/support/RouteParameters",
                "esri/tasks/support/FeatureSet",
                "esri/core/urlUtils",


                "esri/widgets/Legend",
                "esri/layers/FeatureLayer",

                "dojo/domReady!"
            ],
                    function(
                    Locator,
                     Map, dom, on, domClass,  MapView, Graphic, GraphicsLayer, RouteTask, RouteParameters,
                     FeatureSet, urlUtils, Legend,
                     FeatureLayer
                    ) {


                //direction 

                var routeTask = new RouteTask({
                    url: "http://utility.arcgis.com/usrsvcs/servers/4ea1d05bc5314c37a3f253164797023d/rest/services/World/Route/NAServer/Route_World"
                });
                
                // The stops and route result will be stored in this layer
                var routeLyr = new GraphicsLayer();

                // Setup the route parameters
                var routeParams = new RouteParameters({
                    stops: new FeatureSet(),
                    outSpatialReference: { // autocasts as new SpatialReference()
                        wkid: 3857
                    }
                });

                var startSymbol = {
                    type: "picture-marker",
                    url: BASE_URL + "assets/image/start.png",
                    width: "32px",
                    height: "32px"
                };
                var endSymbol = {
                    type: "picture-marker",
                    url: BASE_URL + "assets/image/end.png",
                    width: "32px",
                    height: "32px"
                }; 


                // Define the symbology used to display the route
                var routeSymbol = {
                    type: "simple-line", // autocasts as SimpleLineSymbol()
                    color: [0, 0, 255, 0.5],
                    width: 2
                };
                
               

                




                var legend; 
                var map = new Map({
                    basemap: "streets-navigation-vector",
                    layers: [routeLyr]
                });
                var view = new MapView({
                    container: "viewDiv",  // Reference to the scene div created in step 5
                    map: map,  // Reference to the map object created before the scene
                    zoom: 11,  // Sets zoom level based on level of detail (LOD)    
                    center: [106.637148,-6.170865],
                });

                // Create a locator task using the world geocoding service
                var locatorTask = new Locator({
                    url: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer"
                });


                

                view.when(function(){
                    initFunc();
                    console.log('done');
                });

                //geolocation
                function initFunc() {
                    if( navigator.geolocation ) {
                        navigator.geolocation.getCurrentPosition(function(result){
                            var routeLyr = new GraphicsLayer();
                            var routeParams = new RouteParameters({
                                stops: new FeatureSet(),
                                directionsLengthUnits: 'kilometers',
                                returnDirections: true,
                                findBestSequence: true,
                                outSpatialReference: {
                                    wkid: 4326
                                },
                                restrictionAttributes: ["Preferred for Pedestrians"]
                                
                            });
                            var start = new Graphic({
                                geometry: {
                                    type: "point",
                                    longitude: result.coords.longitude,
                                    latitude: result.coords.latitude
                                },
                                symbol: startSymbol
                            });
                            
                            var x = "<?=$x?>";
                            var y = "<?=$y?>";

                            var end = new Graphic({
                                geometry: {
                                    type: "point",
                                    longitude: x,
                                    latitude: y
                                },
                                symbol: endSymbol
                            });
//                            view.center = [result.coords.longitude, result.coords.latitude];
//                            view.zoom = 8;
                            routeLyr.add(start);
                            routeLyr.add(end);
                            map.layers.add(routeLyr);

                            routeParams.stops.features.push(start, end);
                            routeTask.solve(routeParams).then(function(response){
                                var routeResult = response.routeResults[0].route;
                                var direction = response.routeResults[0].directions;
                                console.log(direction.totalLength);
                                routeResult.symbol = routeSymbol;
                                routeLyr.add(routeResult);
                            });
                        });
                    } else {
                        console.log("Browser doesn't support Geolocation. Visit http://caniuse.com to see browser support for the Geolocation API.");
                    }
                }
                
                //geolocation
                
                
                function errorCallback(){
                    alert('server not responding...');
                }
                
            });

            
        </script>
    