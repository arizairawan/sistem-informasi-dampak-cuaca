<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?=$title?> | Adiwiyata</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/sweetalert.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
page. However, you can choose any other skin. Make sure you
apply the skin class to the body tag so the changes take effect. -->
        <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/skins/skin-blue.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery 3.1.1 -->
        <script src="<?=base_url()?>assets/plugins/jQuery/jquery-3.1.1.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- CK Editor -->
        <script src="<?=base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>
        <!-- DataTables -->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/sweetalert.min.js"></script>

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

        <!-- AdminLTE App -->
        <script src="<?=base_url()?>assets/dist/js/adminlte.min.js"></script>
        <script>
            var base_url = "<?=base_url()?>";
            $(document).ready(function(){
                
                function loadgrafik(){
                    var tgl_awal = $('#tgl_awal').val();
                    var tgl_akhir = $('#tgl_akhir').val();
                    if(tgl_awal != '' && tgl_akhir != ''){
                        $('#myfirstchart').html('');
                        
                        var uri = base_url + 'publik/getdatagrafik/data_curahhujan/'+tgl_awal+'/'+tgl_akhir+'/<?=$tipe?>/<?=$id?>';

                        $.ajax({
                            type: 'POST',
                            url: uri,
                            dataType: 'JSON',
                            success: function(response) {
                                new Morris.Line({
                                    // ID of the element in which to draw the chart.
                                    element: 'myfirstchart',
                                    // Chart data records -- each entry in this array corresponds to a point on
                                    // the chart.
                                    data: response.data,
                                    // The name of the data record attribute that contains x-values.
                                    xkey: 'tgl_lapor',
                                    // A list of names of data record attributes that contain y-values.
                                    ykeys: ['curahhujan'],
                                    // Labels for the ykeys -- will be displayed when you hover over the
                                    // chart.
                                    labels: ['Curah Hujan (mm)']
                                });
                            }
                        })
                    }else{
                        alert('Isilah tanggal untuk melakukan filter')
                    }
                }

                loadgrafik();
                
                $(document).on('click','#filter', function(){
                    loadgrafik();
                });
                
                $(document).on('change','#tgl_awal', function(){
                    var tgl_awal = $('#tgl_awal').val();
                    $('#tgl_akhir').val('');   
                    $('#tgl_akhir').attr('min', tgl_awal);   
                })
                $(document).on('change','#tgl_akhir', function(){
                    var tgl_akhir = $('#tgl_akhir').val();
                    $('#tgl_awal').attr('max', tgl_akhir);   
                })
            })
        </script>
        <style>
            table tr td{
                white-space: nowrap
            }
        </style>
    </head>
    <body>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h4>Nama relawan : <?=$title?> </h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p><b>Alamat relawan : </b><?=$relawan['alamat']?></p>
                                    <p><b>Kecamatan : </b><?=$kecamatan['nama_kecamatan']?></p>
                                    <p><b>Kota : </b><?=$kota['nama_kota']?></p>
                                    <p><b>Email : </b><?=$relawan['email']?></p>
                                </div>
                                <div class="col-sm-6">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header">
                            <form method="post" action="#" class="form-inline">
                                <div class="form-group">
                                    <label>Awal</label>
                                    <input type="date" id="tgl_awal" value="<?=date("Y-m-d", strtotime("-1 month", strtotime(date('Y-m-d'))))?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Akhir</label>
                                    <input type="date" id="tgl_akhir" value="<?=date("Y-m-d")?>" class="form-control">
                                </div>
                                <button type="button" id="filter" class="btn btn-primary">Tampilkan</button>
                            </form>
                        </div>
                        <div class="box-body">
                            <div id="myfirstchart" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
