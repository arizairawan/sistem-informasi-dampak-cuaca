<!-- Main content -->
<section class="content">
    <div class="row">
        <?php if($this->session->userdata('logged') == TRUE && $_SESSION['tipe'] != 'internal'){ ?>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?=date('d M Y')?></h3>

                    <p>Tanggal Hari ini</p>
                </div>
                <div class="icon">
                    <i class="fa fa-clock-o"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?=$jml['jml_lapor']?></h3>

                    <p>Jumlah Lapor Bulan ini</p>
                </div>
                <div class="icon">
                    <i class="fa fa-send"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?=$jml['jml_bolos']?></h3>

                    <p>Tidak Lapor Bulan ini</p>
                </div>
                <div class="icon">
                    <i class="fa fa-times"></i>
                </div>
            </div>
        </div>
        <?php } ?>
        
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body"> 
                    <h2>Selamat Datang di Sistem Informasi Spasial </h2>
                    <h3>Laporan Curah Hujan</h3>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>