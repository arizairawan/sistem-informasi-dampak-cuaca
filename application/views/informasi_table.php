<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModaladd">Tambah</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Judul</th>
                                <th>Tgl Input</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<div class="modal fade" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Tambah</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/informasi/addinformasi" id="formadd" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Judul</label>
                        <input type="text" name="judul_informasi" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Isi Informasi</label>
                        <textarea name="isi_informasi" id="editor1" class="form-control ckeditor" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Edit</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/informasi/editinformasi" id="formedit" enctype="multipart/form-data">
                    <input type="hidden" id="id_informasi" name="id_informasi">
                    <div class="form-group">
                        <label>Judul</label>
                        <input type="text" id="judul_informasi" name="judul_informasi" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Isi Informasi</label>
                        <textarea name="isi_informasi" id="editor2" class="form-control ckeditor" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <br>
                        <div id="img-now"></div>
                        <input type="file" id="customFile" name="image" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>
            
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-now img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on('change',"#customFile",function () {
            readURL(this);
        });
        
        var url_data = "<?=base_url()?>index.php/informasi/loaddata";
        
        $("#example1").DataTable( {
            "ajax": url_data,
            "columns": [
                { "data": "no" },
                {
                    "data": null,
                    "render": function (data) {
                        return '<img src="'+ data.image_dir + '" width="100px">';
                    }
                },
                { "data": "judul_informasi" },
                { "data": "tgl_input" },
                {
                    "data": null,
                    "render": function (data) {
                        return '<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModaledit" data-id="'+ data.id_informasi + '">Edit</button> <button class="btn btn-danger btn-xs hapus" data-id="'+ data.id_informasi + '">Hapus</button>';
                    }
                }
            ]
        });


        $('#myModaledit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var id = button.data('id')
            
            $.ajax({
                type: 'POST',
                url: url_data + '/' + id,
                dataType: 'JSON',
                success: function(response) {
                    $('#id_informasi').val(response.data[0].id_informasi);
                    $('#judul_informasi').val(response.data[0].judul_informasi);
                    $('#img-now').html('<img src="'+response.data[0].image_dir+'" width="200px">');
                    CKEDITOR.instances.editor2.setData( response.data[0].isi_informasi );
                }
            });
        })
        
        $(document).on('submit','#formedit',function(e){
            e.preventDefault();
            var data = new FormData(this);
            //add the deskripsi_event
            data.append('isi_informasi', CKEDITOR.instances['editor2'].getData());
            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });
            $.ajax({
                'type': 'POST',
                'url': $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Diperbaharui",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaledit').modal('hide');
                            $("#formedit")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
        });
        
        $(document).on('submit','#formadd',function(e){
            e.preventDefault();
            var data = new FormData(this);
            //add the deskripsi_event
            data.append('isi_informasi', CKEDITOR.instances['editor1'].getData());
            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });
            $.ajax({
                'type': 'POST',
                'url': $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Ditambahkan",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaladd').modal('hide');
                            $("#formadd")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
        });

        $(document).on('click','.hapus',function () {
            var form_data = {
                id: $(this).attr('data-id'),
            };
            swal({   
                title: "Hapus data",   
                text: "Apakah anda yakin akan menghapus data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, Hapus!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/informasi/hapus",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah dihapus",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    var table = $('#example1').DataTable();
                                    table.ajax.url(url_data).load();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal Dihapus!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses hapus data telah dibatakan", "error");   
                } 
            });
        });
    });
</script>