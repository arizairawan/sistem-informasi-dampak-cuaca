<?php 
$qr = simple_encrypt($relawan['id_relawan']);

$config['cacheable']    = true; //boolean, the default is true
$config['cachedir']     = './assets/'; //string, the default is application/cache/
$config['errorlog']     = './assets/'; //string, the default is application/logs/
$config['imagedir']     = './assets/qrcode/'; //direktori penyimpanan qr code
$config['quality']      = true; //boolean, the default is true
$config['size']         = '1024'; //interger, the default is 1024
$config['black']        = array(255,255,255); // array, default is array(255,255,255)
$config['white']        = array(70,130,180); // array, default is array(0,0,0)

$this->ciqrcode->initialize($config);

$image_name= $qr.'.png'; //buat name dari qr code sesuai dengan nim

$params['data'] = site_url().'printsurat/sk/'.$qr; //data yang akan di jadikan QR CODE
$params['level'] = 'H'; //H=High
$params['size'] = 10;
$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

?>
<html>
    <head>
        <title>SK Adiwiyata</title>
        <style>

            table{
                border-collapse: collapse;
                width: 96%;
                margin: 0 auto;
            }
            table th{
                border:0;
                padding: 3px;
                font-weight: bold;
                text-align: center;
            }
            table td{
                border:0;
                padding: 3px;
                vertical-align: top;
                font-size: 14px;
            }


            .page_break { page-break-after: always; }

            .right{
                float: right
            }
            p{
                text-align: justify
            }
        </style>
    </head>
    <body>
        
        <table>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <br>
                                <img style="width:80px" src="./assets/logokop.png">
                            </td>
                            <td align="center">
                                <h4 style="font-size:17px">BADAN METEOROLOGI KLIMATOLOGI DAN GEOFISIKA</h4>

                                <h4>Jl. Angkasa I No. 2 Kemayoran, Jakarta Pusat, DKI Jakarta 10720 <br> 
                                Telp. : (021) 4246321, Fax : (021) 4246703 P.O Box 3540 JKT, <br>
                                    Website : https://www.bmkg.go.id</h4>
                            </td>
                        </tr>
                    </table>
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    No.	: 023/SK.R/707/<?=$relawan['id_relawan']?>/<?=date('Y',strtotime($hasil['tgl_validasi']))?> <br>
                    Perihal	: SK Relawan BMKG
                </td>
                <td align="right">Jakarta, <?=date('d M Y',strtotime($hasil['tgl_validasi']))?></td>
            </tr>
            <tr>
                <td colspan="2" align="justify">
                    <br>
                    <br>
                    <br>
                    <br>
                    <p>
                        Bersamaan dengan keputusan Kepala Pusat Meteorologi Publik Badan Meteorologi Klimatologi dan Geofisika No. 003/KS.M/053/03/2007, tentang kerjasama penelitian bersama masyarakat sipil. Kami memutuskan bahwa Calon Relawan yang tersebut dibawah ini :
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>ID Relawan</td>
                            <td> : <?=$relawan['id_relawan']?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td> : <?=$relawan['nama_relawan']?></td>
                        </tr>
                        <tr>
                            <td>No. KTP</td>
                            <td> : <?=$relawan['no_ktp']?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td> : <?=$relawan['alamat']?>
                            </td>
                        </tr>
                        <tr>
                            <td>Telepon</td>
                            <td> : <?=$relawan['telepon']?></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="justify">
                    <p>
                        Memang benar terdaftar sebagai RELAWAN yang bertugas dibawah pengawasan Pusat Meteorologi Publik Badan Meteorologi Klimatologi dan Geofisika.
                    </p>
                </td>
            </tr>



            <tr>
                <td>
                    <br>
                    <br>
                    <img src="./assets/qrcode/<?=$qr?>.png" height="100px">
                </td>
                <td align="right">
                    <br>
                    <br>
                    Ditetapkan di Jakarta <br>
                    Pada Tanggal   <?=date('d M Y',strtotime($hasil['tgl_validasi']))?> <br>
                    <br>
                    <b>KEPALA PUSAT METEOROLOGI PUBLIK </b> <br>

                    <br>
                    <br>
                    [TTD]
                    <br>
                    <br>


                    <b>A. FACHRI RADJAB, S.Si, M.Si</b>	  
                    <br>NIP.196307021984102006
                </td>
            </tr>
        </table>


    </body>
</html>