<!-- Main content -->
<section class="content">
    <div class="row">       
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModaladd">Tambah </a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Suhu Min (C)</th>
                                <th>Suhu Max (C)</th>
                                <th>Suhu Avg (C)</th>
                                <th>Lampiran</th>
                                <th>Tanggal</th>
                                <th>Verifikasi</th>
                                <th>Validasi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Tambah</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/laporstasiun/addsuhu" id="formadd" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Suhu Min (Celsius)</label>
                        <input type="text" name="suhu_min" class="form-control inputfloat" required>
                    </div>
                    <div class="form-group">
                        <label>Suhu Max (Celsius)</label>
                        <input type="text" name="suhu_max" class="form-control inputfloat" required>
                    </div>
                    <div class="form-group">
                        <label>Suhu Avg (Celsius)</label>
                        <input type="text" name="suhu_avg" class="form-control inputfloat" required>
                    </div>
                    <div class="form-group">
                        <label>Lampiran</label>
                        <input type="file" name="lampiran" class="form-control" required>
                    </div>

                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Edit</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/laporstasiun/editsuhu" id="formedit" enctype="multipart/form-data">
                    <input type="hidden" name="id_suhu" id="id_suhu">
                    <div class="form-group">
                        <label>Suhu Min (Celsius)</label>
                        <input type="text" name="suhu_min" id="min" class="form-control inputfloat" required>
                    </div>
                    <div class="form-group">
                        <label>Suhu Max (Celsius)</label>
                        <input type="text" name="suhu_max" id="max" class="form-control inputfloat" required>
                    </div>
                    <div class="form-group">
                        <label>Suhu Avg (Celsius)</label>
                        <input type="text" name="suhu_avg" id="avg" class="form-control inputfloat" required>
                    </div>
                    <div class="form-group">
                        <label>Lampiran</label>
                        <input type="file" name="lampiran" class="form-control">
                        <i>Kosongkan lampiran jika tidak ingin mengganti lampiran</i>
                    </div>

                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        
        var url_data2 = base_url + "index.php/laporstasiun/loaddatasuhu/<?=simple_encrypt($_SESSION['id_user'])?>";

        $("#example1").DataTable( {
            "ajax": url_data2,
            "scrollX" : true,
            "columns": [
                { "data": "no" },
                { "data": "suhu_min" },
                { "data": "suhu_max" },
                { "data": "suhu_avg" },
                {
                    "data": null,
                    "render": function (data) {
                        return '<a class="btn btn-info btn-xs" target="_blank" href="'+base_url+'assets/lampiran/'+ data.lampiran + '">Lihat Lampiran</a>';
                    }
                },
                { "data": "tgl_lapor" },
                { "data": "verifikasi" },
                { "data": "validasi" },
                {
                    "data": null,
                    "render": function (data) {
                        if(data.status_verifikasi == 'N'){
                            return '<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModaledit" data-id="'+ data.id_suhu + '" data-n="'+ data.suhu_min + '" data-x="'+ data.suhu_max + '" data-a="'+ data.suhu_avg + '">Edit</button>';
                        }else{
                            return '-';
                        }
                    }
                }
            ]
        });


        $('#myModaledit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var id = button.data('id')
            var n = button.data('n')
            var x = button.data('x')
            var a = button.data('a')

            $('#id_suhu').val(id);
            $('#min').val(n);
            $('#max').val(x);
            $('#avg').val(a);
        })

        $(document).on('submit','#formedit',function(e){
            e.preventDefault();
            var data = new FormData(this);

            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Diperbaharui",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaledit').modal('hide');
                            $("#formedit")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data2).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
        });

        $(document).on('submit','#formadd',function(e){
            e.preventDefault();
            var data = new FormData(this);

            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    console.log(response)
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Ditambahkan",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaladd').modal('hide');
                            $("#formadd")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data2).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
        });

        $(document).on('click','.hapus',function () {
            var form_data = {
                id: $(this).attr('data-id')
            };
            swal({   
                title: "Hapus data",   
                text: "Apakah anda yakin akan menghapus data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, Hapus!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/curahhujan/hapus",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah dihapus",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    var table = $('#example1').DataTable();
                                    table.ajax.url(url_data).load();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal Dihapus!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses hapus data telah dibatakan", "error");   
                } 
            });
        });


    });
</script>