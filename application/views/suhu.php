<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="" class="form-inline">
                        <div class="form-group">
                            <label>Filter</label>
                            <input type="date" name="tgl1" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="date" name="tgl2" class="form-control">
                        </div>
                        <button class="btn btn-primary">Tampilkan</button>
                    </form>
                    <br>
                    <table id="example1" class="table table-bordered table-striped nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Suhu Min</th>
                                <th>Suhu Max</th>
                                <th>Suhu Avg</th>
                                <th>Lampiran</th>
                                <th>Stasiun</th>
                                <th>Kecamatan</th>
                                <th>Tanggal Input</th>
                                <th>Verifikasi</th>
                                <th>Validasi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<script>
    $(document).ready(function(){
        $(document).on('click','.verifikasi',function () {
            var form_data = {
                id: $(this).attr('data-id'),
            };
            swal({   
                title: "Verifikasi data",   
                text: "Apakah anda yakin akan memverifikasi data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, Verifikasi!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/cuaca/verifikasi3",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah diverifikasi",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal diverifikasi!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses verifikasi data telah dibatakan", "error");   
                } 
            });
        }); 

        $(document).on('click','.validasi',function () {
            var form_data = {
                id: $(this).attr('data-id'),
            };
            swal({   
                title: "validasi data",   
                text: "Apakah anda yakin akan memvalidasi data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, validasi!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/cuaca/validasi3",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah divalidasi",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal divalidasi!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses validasi data telah dibatakan", "error");   
                } 
            });
        }); 

        var url_data = base_url + "index.php/cuaca/loaddatasuhu/";

        var level = <?=$_SESSION['level']?>;

        $("#example1").DataTable( {
            "ajax": url_data,
            "scrollX" : true,
            "columns": [
                { "data": "no" },
                { "data": "suhu_min" },
                { "data": "suhu_max" },
                { "data": "suhu_max" },
                {
                    "data": null,
                    "render": function (data) {
                        return '<a class="btn btn-info btn-xs" target="_blank" href="'+base_url+'assets/lampiran/'+ data.lampiran + '">Lihat Lampiran</a>';
                    }
                },
                { "data": "nama_stasiun" },
                { 
                    "data": null,
                    "render": function(data){
                        return data.nama_kecamatan + ' / ' + data.nama_kota;
                    }
                },
                { "data": "tgl_lapor" },
                { "data": "verifikasi" },
                { "data": "validasi" },
                {
                    "data": null,
                    "render": function (data) {
                        if(level == 2 && data.status_verifikasi == 'N'){
                            return '<button class="btn btn-warning btn-xs verifikasi" data-id="'+ data.id_suhu + '">Verifikasi</button>';
                        }else if(level == 3 && data.status_verifikasi == 'Y' && data.status_validasi == 'N'){
                            return '<button class="btn btn-warning btn-xs validasi" data-id="'+ data.id_suhu + '">Validasi</button>';
                        }else{
                            return '-';
                        }
                    }
                }
            ]
        });

    });
</script>