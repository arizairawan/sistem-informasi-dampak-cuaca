<style>
    .tblist th,.tblist td{
        padding: 5px
    }
</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <table class="tblist">
                                <tr>
                                    <th>Data </th>
                                    <td>: <?=$request['data']?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Data </th>
                                    <td>: <?=$request['tgl_awal']." - ".$request['tgl_akhir']?></td>
                                </tr>
                                <tr>
                                    <th>Kota </th>
                                    <td>: <?=$kota?></td>
                                </tr>
                                <tr>
                                    <th>Kecamatan </th>
                                    <td>: <?=$kecamatan?></td>
                                </tr>
                                <tr>
                                    <th>Tujuan Penggunaan </th>
                                    <td>: <?=$request['tujuan_penggunaan']?></td>
                                </tr>
                                <tr>
                                    <th>Oleh </th>
                                    <td>: <?=$oleh['nama_user']?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Request </th>
                                    <td>: <?=$request['tgl_request']?></td>
                                </tr>
                                <tr>
                                    <th>Surat Permohonan</th>
                                    <td>: <a target="_blank" href="<?=base_url().'assets/lampiran/'.$request['surat_permohonan']?>"><?=$request['surat_permohonan']?></a></td>
                                </tr>
                                <tr>
                                    <th>Verifikasi </th>
                                    <td>: <?=$verifikasi?></td>
                                </tr>
                                <tr>
                                    <th>Verifikasi </th>
                                    <td>: <?=$validasi?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <?php if($request['status_validasi'] == 'Y' && $_SESSION['level'] == 4){ ?>
                            <a target="_blank" href="<?=base_url()?>request/download/<?=simple_encrypt($request['id_request'])?>" class="btn btn-success btn-lg">Download Data</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="myModalupload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Hasil Pelatihan</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="formuploadhasil" action="<?=base_url()?>relawan/uploadhasilpelatihan" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>File Hasil Pelatihan</label>
                        <input type="file" name="hasil_pelatihan" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </form>
            </div>
        </div>
    </div>
</div>

