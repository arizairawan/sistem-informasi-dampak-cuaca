<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <?php if($_SESSION['level'] <= 2){ ?>
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal">Tambah Stasiun</button>
                        </div>
                        <?php } ?>
                        <div class="col-sm-10">
                            <form class="form-inline" action="#" id="formfilter">
                                <div class="form-group">
                                    <select class="form-control" name="id_upt" id="kota">
                                        <option value="all"> - Kota -</option>
                                        <?php foreach($kota as $kotax): ?>
                                        <option value="<?=$kotax['nama_kota']?>" data-id="<?=$kotax['id_kota']?>"><?=$kotax['nama_kota']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <script>
                                    $(document).on('change','#kota', function(){
                                        var id_kota = $('#kota option:selected').attr('data-id');
                                        $('#kec option.opsi').css('display','none');
                                        $("#kec option[data-kota="+id_kota+"]").css('display','block');
                                    });   
                                </script>
                                <div class="form-group">
                                    <select class="form-control" name="id_tingkat_adiwiyata" id="kec">
                                        <option value="all"> - Kecamatan -</option>
                                        <?php foreach($kecamatan as $kec): ?>
                                        <option class="opsi" value="<?=$kec['nama_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kecamatan</th>
                                <th>Kota</th>
                                <th>Telepon</th>
                                <th>Email</th>
                                <th>Keterangan</th>
                                <th>Tgl Input</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Stasiun</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>stasiun/prosesadd" id="formadd">
                    <div class="form-group row">
                        <label class="col-sm-4">Nama Stasiun</label>
                        <div class="col-sm-8">
                            <input type="text" name="nama_stasiun" required placeholder="Nama Stasiun" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4">Alamat</label>
                        <div class="col-sm-8">
                            <textarea name="alamat" required class="form-control" placeholder="Alamat"></textarea>
                        </div> 
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4">Kota</label>
                        <div class="col-sm-8">
                            <select class="form-control" required name="id_kota" id="id_kota">
                                <option value="">- Kota -</option>
                                <?php foreach($kota as $ko): ?>
                                <option value="<?=$ko['id_kota']?>"><?=$ko['nama_kota']?></option>
                                <?php endforeach; ?>
                            </select> 
                        </div> 
                    </div>
                    <script>
                        $(document).on('change','#id_kota', function(){
                            var id_kota = $(this).val();
                            $('#id_kecamatan option.opsi').css('display','none');
                            $("#id_kecamatan option[data-kota="+id_kota+"]").css('display','block');
                        });   
                    </script>
                    <div class="form-group row">
                        <label class="col-sm-4">Kecamatan</label>
                        <div class="col-sm-8">
                            <select class="form-control" required name="id_kecamatan" id="id_kecamatan">
                                <option value="">- Kecamatan -</option>
                                <?php foreach($kecamatan as $kec): ?>
                                <option class="opsi" value="<?=$kec['id_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                                <?php endforeach; ?>
                            </select> 
                        </div> 
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4">Telepon</label>
                        <div class="col-sm-8">
                            <input type="text" name="telepon" required class="form-control" placeholder="Telepon">   
                        </div> 
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4">Email</label>
                        <div class="col-sm-8">
                            <input type="email" name="email" required class="form-control" placeholder="Email">   
                        </div> 
                    </div>
                    <div id="pass" class="form-group row">
                        <label class="col-sm-4">Password</label>
                        <div class="col-sm-8">
                            <input type="password" name="password1" required class="form-control" placeholder="Password">   
                        </div> 
                    </div>
                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        var url_data = "<?=base_url()?>index.php/stasiun/loaddata";


        var table = $("#example1").DataTable( {
            "ajax": url_data,
            "scrollX" : true,
            "columns": [
                { "data": "no" },
                { "data": "nama_stasiun" },
                { "data": "nama_kecamatan" },
                { "data": "nama_kota" },
                { "data": "telepon" },
                { "data": "email" },
                {
                    "data": null,
                    "render": function (data) {
                        if(data.jml.jml_bolos >= 3){
                            var jml = '<b class="text-danger">' + data.jml.jml_bolos + ' hari tidak lapor bulan ini</b>';
                        }if(data.jml.jml_bolos == 0){
                            var jml = '<b class="text-danger">-</b>';
                        }else{
                            var jml = '<b class="text-danger">' + data.jml.jml_bolos + ' hari tidak lapor bulan ini</b>';
                        }
                        return jml;
                    }
                },
                { "data": "tgl_input" },
                {
                    "data": null,
                    "render": function (data) {
                        return '<a class="btn btn-warning btn-xs" href="stasiun/detail/'+ data.id_stasiun + '">Detail</a>';
                    }
                }
            ],

        });


        $(document).on('change','#kec',function(){
            var ts = $(this).val();
            table.columns( 3 ).search( ts ).draw();
        });

        $(document).on('change','#kota',function(){
            var upt = $(this).val();
            table.columns( 4 ).search( upt ).draw();
        });
        
        
        $('#formadd').submit(function() {
            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Ditambahkan",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaladd').modal('hide');
                            $("#formadd")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
            return false;
        });
    });
</script>