<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sistem Informasi Spasial Curah Hujan | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <style>
            .login-box{
                background: #ccc
            }
        </style>
    </head>
    <body class="hold-transition login-page" style="padding-top:130px">
        <div class="col-sm-offset-4 col-sm-4">
            <div class="login-logo">
                <a href="<?=base_url()?>assets/index2.html">Sistem Informasi Spasial Curah Hujan</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <div class="nav-tabs-custom">
                    
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Relawan/St.</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Internal</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <form action="<?=base_url()?>login/relawan" class="formsignin" method="post">
                                <div class="form-group has-feedback">
                                    <input type="email" class="form-control" name="email" placeholder="Email">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat kliklogin">Login</button>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </form>
                            <br>
                            <div class="text-center">
                                <a href="<?=base_url()?>registrasi">Registrasi Relawan</a>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <form action="<?=base_url()?>login/internal" class="formsignin" method="post">
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" name="username" placeholder="Username">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="row">
                                    <!-- /.col -->
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat kliklogin">Login</button>
                                    </div>

                                    <!-- /.col -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3.1.1 -->
        <script src="<?=base_url()?>assets/plugins/jQuery/jquery-3.1.1.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                $('.formsignin').submit(function() {
                    $( document ).ajaxStart(function() {
                        $( ".kliklogin" ).text('Loading...');
                    }).ajaxStop(function() {
                        $( ".kliklogin" ).text('Login');
                    });

                    $.ajax({
                        type: 'POST',
                        url: $(this).attr('action'),
                        data: $(this).serialize(),
                        success: function(response) {
                            if(response == 'success'){
                                location.href = 'home';
                            }else if(response == 'suspend'){
                                alert('Akun anda telah ditangguhkan, untuk lebih lanjut silakan hubungi admin')
                            }else{
                                alert('Akun dan Password anda tidak sesuai, silakan ulangi kembali')
                                //                                swal('Sorry','Error','error');
                            }
                        }
                    });
                    return false;
                });

            });
        </script>
    </body>
</html>
