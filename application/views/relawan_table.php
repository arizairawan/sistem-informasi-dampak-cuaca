<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <form class="form-inline" action="#" id="formfilter">
                        <div class="form-group">
                            <select class="form-control" name="id_kota" id="kota">
                                <option value="all"> - Kota -</option>
                                <?php foreach($kota as $kotax): ?>
                                <option value="<?=$kotax['nama_kota']?>" data-id="<?=$kotax['id_kota']?>"><?=$kotax['nama_kota']?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <script>
                            $(document).on('change','#kota', function(){
                                var id_kota = $('#kota option:selected').attr('data-id');
                                $('#kec option.opsi').css('display','none');
                                $("#kec option[data-kota="+id_kota+"]").css('display','block');
                            });   
                        </script>
                        <div class="form-group">
                            <select class="form-control" name="id_kecamatan" id="kec">
                                <option value="all"> - Kecamatan -</option>
                                <?php foreach($kecamatan as $kec): ?>
                                <option class="opsi" value="<?=$kec['nama_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        
                    </form>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>No KTP</th>
                                <th>Kecamatan</th>
                                <th>Kota</th>
                                <th>Telepon</th>
                                <th>Email</th>
                                <th>Keterangan</th>
                                <th>Verifikasi</th>
                                <th>Validasi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


<script>
    $(document).ready(function() {

        var url_data = "<?=base_url()?>index.php/relawan/loaddata";
        var url_data2 = "<?=base_url()?>index.php/relawan/filterdata";

      
        var table = $("#example1").DataTable( {
            "ajax": url_data,
            "scrollX" : true,
            "columns": [
                { "data": "no" },
                { "data": "nama_relawan" },
                { "data": "no_ktp" },
                { "data": "nama_kecamatan" },
                { "data": "nama_kota" },
                { "data": "telepon" },
                { "data": "email" },
                {
                    "data": null,
                    "render": function (data) {
                        if(data.jml.jml_bolos >= 3){
                            if(data.suspend_relawan == 'Y'){
                                var jml = '<b class="text-danger">' + data.jml.jml_bolos + ' hari tidak lapor bulan ini</b> <br>Akun ditangguhkan pada ' + data.tgl_suspend;
                            }else{
                                var jml = '<b class="text-danger">' + data.jml.jml_bolos + ' hari tidak lapor bulan ini</b>';
                            }
                        }else if(data.jml.jml_bolos == 0){
                            var jml = '<b class="text-danger">-</b>';
                        }else{
                            var jml = '<b class="text-danger">' + data.jml.jml_bolos + ' hari tidak lapor bulan ini</b>';
                        }
                        return jml;
                    }
                },
                { "data": "status_vr" },
                { "data": "status_vl" },
                {
                    "data": null,
                    "render": function (data) {
                        return '<a class="btn btn-warning btn-xs" href="relawan/detail/'+ data.id_relawan + '">Detail</a>';
                    }
                }
            ],
            
        });

        
        $(document).on('change','#kec',function(){
            var ts = $(this).val();
            table.columns( 3 ).search( ts ).draw();
        });
        
        $(document).on('change','#kota',function(){
            var upt = $(this).val();
            table.columns( 4 ).search( upt ).draw();
        });
        
      
        
 
    });
</script>