<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php if($_SESSION['level'] <= 2){ ?>
                    <form action="<?=base_url()?>stasiun/prosesedit" id="formedit" method="post" enctype="multipart/form-data">
                        <?php } else { ?>
                        <form method="post" action="#">
                        <?php } ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Update Profil Stasiun</h4>
                                <hr>
                            </div>
                            
                            <input type="hidden" name="id_stasiun" value="<?=$stasiun['id_stasiun']?>">
                            <div class="col-sm-6">                    
                                <div class="form-group row">
                                    <label class="col-sm-4">Nama Stasiun</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="nama_stasiun" value="<?=$stasiun['nama_stasiun']?>" required class="form-control" placeholder="Nama stasiun">   
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4">Alamat</label>
                                    <div class="col-sm-8">
                                        <textarea name="alamat" required class="form-control" placeholder="Alamat"><?=$stasiun['alamat']?></textarea>
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4">Kota</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" required name="id_kota" id="id_kota">
                                            <option value="">- Kota -</option>
                                            <?php foreach($kota as $ko): ?>
                                            <option value="<?=$ko['id_kota']?>"><?=$ko['nama_kota']?></option>
                                            <?php endforeach; ?>
                                        </select> 
                                    </div> 
                                </div>
                                <script>
                                    $(function(){
                                        $("#id_kecamatan option[value=<?=$stasiun['id_kecamatan']?>]").css('display','block');
                                        $("#id_kecamatan option[value=<?=$stasiun['id_kecamatan']?>]").attr('selected',true);
                                        var idkotaselected = $("#id_kecamatan option[value=<?=$stasiun['id_kecamatan']?>]").attr('data-kota');
                                        $("#id_kota option[value="+idkotaselected+"]").attr('selected',true);
                                    });
                                    $(document).on('change','#id_kota', function(){
                                        var id_kota = $(this).val();
                                        $('#id_kecamatan option').removeAttr('selected');
                                        $('#id_kecamatan option.opsi').css('display','none');
                                        $("#id_kecamatan option[data-kota="+id_kota+"]").css('display','block');
                                    });   
                                </script>
                                <div class="form-group row">
                                    <label class="col-sm-4">Kecamatan</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" required name="id_kecamatan" id="id_kecamatan">
                                            <option value="">- Kecamatan -</option>
                                            <?php foreach($kecamatan as $kec): ?>
                                            <option class="opsi" value="<?=$kec['id_kecamatan']?>" data-kota="<?=$kec['id_kota']?>" style="display:none"><?=$kec['nama_kecamatan']?></option>
                                            <?php endforeach; ?>
                                        </select> 
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4">Telepon</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="telepon" value="<?=$stasiun['telepon']?>" required class="form-control" placeholder="Telepon">   
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4">Email</label>
                                    <div class="col-sm-8">
                                        <input type="email" name="email" value="<?=$stasiun['email']?>" required class="form-control" placeholder="Email">   
                                    </div> 
                                </div>
                                <hr>
                                <div id="pass" class="form-group row">
                                    <label class="col-sm-4">Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" name="password" class="form-control" placeholder="Password">  
                                        <i>Kosongkan Password jika tidak ingin mengubah password</i>
                                    </div> 
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-sm-2">
                                <?php if($_SESSION['level'] <= 2){ ?>
                                <button type="submit" class="btn btn-primary btn-block btn-flat klik">Submit</button>
                                <?php } ?>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

    $(document).on('submit','#formedit',function(e){
        e.preventDefault();
        var data = new FormData(this);

        $( document ).ajaxStart(function() {
            $( ".klik" ).text('Loading...');
        }).ajaxStop(function() {
            $( ".klik" ).text('Submit');
        });
        $.ajax({
            'type': 'POST',
            'url': $(this).attr('action'),
            'data': data,
            'processData': false,
            'contentType': false,
            'cache': false,
            success: function(response) {
                if(response == 'success'){
                    swal({
                        title: "Update Data Berhasil",
                        type: "success",
                        confirmButtonText: "Okay",
                        closeOnConfirm: true
                    },
                         function(){
                        history.back(1);
                    });
                }
                else if(response == 'password'){
                    swal("Maaf!", "Password tidak sesuai!", "error");
                    $('#pass input').val('');
                    $('#pass2 input').val('');
                    $('#pass').addClass('has-warning');
                    $('#pass2').addClass('has-warning');
                }
                else{
                    swal("Maaf!", "Update Data Gagal!", "error")
                }
            }
        });
    });

</script>