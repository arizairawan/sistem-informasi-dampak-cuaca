<?php 
$qr = simple_encrypt($request['id_request']);

$config['cacheable']    = true; //boolean, the default is true
$config['cachedir']     = './assets/'; //string, the default is application/cache/
$config['errorlog']     = './assets/'; //string, the default is application/logs/
$config['imagedir']     = './assets/qrcode/'; //direktori penyimpanan qr code
$config['quality']      = true; //boolean, the default is true
$config['size']         = '1024'; //interger, the default is 1024
$config['black']        = array(255,255,255); // array, default is array(255,255,255)
$config['white']        = array(70,130,180); // array, default is array(0,0,0)

$this->ciqrcode->initialize($config);

$image_name= $qr.'.png'; //buat name dari qr code sesuai dengan nim

$params['data'] = site_url().'request/download/'.$qr; //data yang akan di jadikan QR CODE
$params['level'] = 'H'; //H=High
$params['size'] = 10;
$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Report Table</title>
        <style type="text/css">
            #outtable{
                border:1px solid #e3e3e3;
                width:100%;
                border-radius: 5px;
            }
            .short{
                width: 50px;
            }
            .normal{
                width: 150px;
            }
            table{
                width: 100%;
                border-collapse: collapse;
                font-family: arial;
                color:#5E5B5C;
            }
            thead th{
                text-align: left;
                padding: 10px;
            }
            tbody td{
                border-top: 1px solid #e3e3e3;
                padding: 10px;
            }
            tbody tr:nth-child(even){
                background: #F6F5FA;
            }
            tbody tr:hover{
                background: #EAE9F5
            }
            .page_break { page-break-after: always; }
        </style>
    </head>
    <body>
        

        <table>
            <tr>
                <td width="80px">
                    <br>
                    <img style="width:80px" src="./assets/logokop.png">
                </td>
                <td align="center">
                    <h4 style="font-size:17px">PELAYANAN TERPADU SATU PINTU <br> BADAN METEOROLOGI KLIMATOLOGI DAN GEOFISIKA</h4>

                    <h4>Jl. Angkasa I No. 2 Kemayoran, Jakarta Pusat, DKI Jakarta 10720 <br> 
                        Telp. : (021) 4246321, Fax : (021) 4246703 P.O Box 3540 JKT, <br>
                        Website : https://www.bmkg.go.id</h4>
                </td>
            </tr>
        </table>
        <hr>
        
        <table>
            <tr>
                <td>
                    No. &nbsp;&nbsp; &nbsp;&nbsp;: 023/PTSP/707/<?=$request['id_request']?>/<?=date('Y',strtotime($request['tgl_validasi']))?> <br>
                    Perihal	: Data Curah Hujan di <?=$daerah?> pada <?=$request['tgl_awal']." s/d ".$request['tgl_akhir']?>
                </td>
                <td align="right">
                <?=date('d M Y',strtotime($request['tgl_validasi']))?>
                </td>
            </tr>
        </table>
    
                    
        <div id="outtable">
            <table>
                <thead>
                    <tr>
                        <th class="short">#</th>
                        <th class="normal">Curah hujan</th>
                        <th class="normal">Tanggal</th>
                        <th class="normal">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; ?>
                    <?php foreach($getdata as $d): 
                    $cekstandar = $this->m_global->cek_standar(round($d['avgch']));

                    $ckst = 'Tidak Hujan';
                    if($cekstandar['nama_kategori'] != null){
                        $ckst = $cekstandar['nama_kategori'];
                    }
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo round($d['avgch']); ?></td>
                        <td><?php echo $d['tgl_lapor']; ?></td>
                        <td><?php echo $ckst; ?></td>
                    </tr>
                    <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        
        <table>
              <tr>
                <td>
                    <br>
                    <br>
                    <img src="./assets/qrcode/<?=$qr?>.png" height="100px">
                </td>
                <td align="right">
                    <br>
                    <br>
                    Ditetapkan di Jakarta <br>
                    Tanggal   <?=date('d M Y',strtotime($request['tgl_validasi']))?> <br>
                    <br>
                    <b>KEPALA PUSAT METEOROLOGI PUBLIK </b> <br>

                    <br>
                    <br>
                    [TTD]
                    <br>
                    <br>


                    <b>A. FACHRI RADJAB, S.Si, M.Si</b>	  
                    <br>NIP.196307021984102006
                </td>
            </tr>
        </table>
    </body>
</html>