<div class="row">
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?=$relawan?></h3>

                <p>Jumlah Relawan</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?=$stasiun?></h3>

                <p>Jumlah Stasiun</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?=$request?></h3>

                <p>Jumlah Request</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
        </div>
    </div>

</div>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4>Curah Hujan Rata-rata Per Bulan</h4>
                        </div>
                        <div class="col-sm-3">
                            <select id="gantitahun" class="form-control">
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019" selected>2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="myfirstchart"  style="height: 250px;"></div>

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<script>
    $(document).ready(function(){

        function loadgrafik(tahun){
            $('#myfirstchart').html('');

            var uri = base_url + 'laporan/loaddata/'+tahun;

            $.ajax({
                type: 'POST',
                url: uri,
                dataType: 'JSON',
                success: function(response) {
                    new Morris.Line({
                        // ID of the element in which to draw the chart.
                        element: 'myfirstchart',
                        // Chart data records -- each entry in this array corresponds to a point on
                        // the chart.
                        data: response,
                        // The name of the data record attribute that contains x-values.
                        xkey: 'x',
                        // A list of names of data record attributes that contain y-values.
                        ykeys: ['rata'],
                        // Labels for the ykeys -- will be displayed when you hover over the
                        // chart.
                        labels: ['Rata-rata (mm)']
                    });
                }
            })
        }

        loadgrafik(2019);

        $(document).on('change','#gantitahun', function(){
            var th = $(this).val();
            loadgrafik(th);
        })

    })
</script>