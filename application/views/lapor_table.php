<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <?php if($relawan['status_validasi'] == 'Y'){ ?>

                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModaladd">Tambah</button>

                    <?php } else { echo "<div class='text-warning'><i class='fa fa-clock-o'></i> Dalam Proses Validasi Akun</div>"; } ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Curah Hujan (mm)</th>
                                <th>Lampiran</th>
                                <th>Tanggal</th>
                                <th>Verifikasi</th>
                                <th>Validasi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<div class="modal fade" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Tambah</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/lapor/addcurahhujan" id="formadd" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Curah Hujan (mm)</label>
                        <input type="number" name="curahhujan" class="form-control inputfloat" required>
                    </div>
                    <div class="form-group">
                        <label>Lampiran</label>
                        <input type="file" name="lampiran" class="form-control" required>
                    </div>

                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Edit</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/lapor/editcurahhujan" id="formedit" enctype="multipart/form-data">
                    <input type="hidden" id="id_curahhujan" name="id_curahhujan">
                    <div class="form-group">
                        <label>Curah Hujan (mm)</label>
                        <input type="text" id="curahhujan" name="curahhujan" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Lampiran</label>
                        <input type="file" name="lampiran" class="form-control">
                        <i>Kosongkan lampiran jika tidak ingin mengganti lampiran</i>
                    </div>
                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        var url_data = base_url + "index.php/lapor/loaddata/<?=simple_encrypt($_SESSION['id_user'])?>";



        $("#example1").DataTable( {
            "ajax": url_data,
            "scrollX" : true,
            "columns": [
                { "data": "no" },
                { "data": "curahhujan" },
                {
                    "data": null,
                    "render": function (data) {
                        return '<a class="btn btn-info btn-xs" target="_blank" href="'+base_url+'assets/lampiran/'+ data.lampiran + '">Lihat Lampiran</a>';
                    }
                },
                { "data": "tgl_lapor" },
                { "data": "verifikasi" },
                { "data": "validasi" },
                {
                    "data": null,
                    "render": function (data) {
                        if(data.status_verifikasi == 'N'){
                            return '<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModaledit" data-id="'+ data.id_curahhujan + '"  data-v="'+ data.curahhujan + '">Edit</button>';
                        }else{
                            return '-';
                        }
                    }
                }
            ]
        });


        $('#myModaledit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var id = button.data('id')
            var v = button.data('v')

            $('#id_curahhujan').val(id);
            $('#curahhujan').val(v);
        })

        $(document).on('submit','#formedit',function(e){
            e.preventDefault();
            var data = new FormData(this);
            
            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Diperbaharui",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaledit').modal('hide');
                            $("#formedit")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
        });

        $(document).on('submit','#formadd',function(e){
            e.preventDefault();
            var data = new FormData(this);

            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                'data': data,
                'processData': false,
                'contentType': false,
                'cache': false,
                success: function(response) {
                    console.log(response)
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Ditambahkan",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaladd').modal('hide');
                            $("#formadd")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
        });

        $(document).on('click','.hapus',function () {
            var form_data = {
                id: $(this).attr('data-id')
            };
            swal({   
                title: "Hapus data",   
                text: "Apakah anda yakin akan menghapus data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, Hapus!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/curahhujan/hapus",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah dihapus",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    var table = $('#example1').DataTable();
                                    table.ajax.url(url_data).load();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal Dihapus!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses hapus data telah dibatakan", "error");   
                } 
            });
        });


    });
</script>