<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModaladd">Tambah</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<div class="modal fade" id="myModaladd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Tambah</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/kota/addkota" id="formadd">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama_kota" class="form-control" required>
                    </div>
                    
                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Form Edit</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="<?=base_url()?>index.php/kota/editkota" id="formedit">
                    <input type="hidden" id="id_kota" name="id_kota">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" id="nama_kota" name="nama_kota" class="form-control" required>
                    </div>
                    
                    <button type="submit" class="btn btn-primary klik">Submit</button>
                </form>
            </div>
            
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        
        var url_data = "<?=base_url()?>index.php/kota/loaddata";
        
        $("#example1").DataTable( {
            "ajax": url_data,
            "scrollX" : true,
            "columns": [
                { "data": "no" },
                { "data": "nama_kota" },
                {
                    "data": null,
                    "render": function (data) {
                        return '<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModaledit" data-id="'+ data.id_kota + '">Edit</button> <button class="btn btn-danger btn-xs hapus" data-id="'+ data.id_kota + '">Hapus</button>';
                    }
                }
            ]
        });


        $('#myModaledit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var id = button.data('id')
            
            $.ajax({
                type: 'POST',
                url: url_data + '/' + id,
                dataType: 'JSON',
                success: function(response) {
                    $('#id_kota').val(response.data[0].id_kota);
                    $('#nama_kota').val(response.data[0].nama_kota);
                }
            });
        })
        
        $('#formedit').submit(function() {
            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Diperbaharui",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaledit').modal('hide');
                            $("#formedit")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
            return false;
        });
        
        $('#formadd').submit(function() {
            $( document ).ajaxStart(function() {
                $( ".klik" ).text('Loading...');
            }).ajaxStop(function() {
                $( ".klik" ).text('Submit');
            });

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(response) {
                    if(response == 'success'){
                        swal({
                            title: "Data Berhasil Ditambahkan",
                            type: "success",
                            confirmButtonText: "Okay",
                            closeOnConfirm: true
                        },
                             function(){
                            $('#myModaladd').modal('hide');
                            $("#formadd")[0].reset();
                            var table = $('#example1').DataTable();
                            table.ajax.url(url_data).load();
                        });
                    }
                    else{
                        swal("Maaf!", "Data Gagal Ditambahkan!", "error")
                    }
                }
            });
            return false;
        });

        $(document).on('click','.hapus',function () {
            var form_data = {
                id: $(this).attr('data-id')
            };
            swal({   
                title: "Hapus data",   
                text: "Apakah anda yakin akan menghapus data ini?",    
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya, Hapus!",   
                cancelButtonText: "Tidak, Batalkan!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: "<?=base_url()?>index.php/kota/hapus",
                        data: form_data,
                        success: function (response) {
                            if(response == 'success'){
                                swal({
                                    title: "Data telah dihapus",
                                    type: "success",
                                    confirmButtonText: "Okay",
                                    closeOnConfirm: true
                                },
                                     function(){
                                    var table = $('#example1').DataTable();
                                    table.ajax.url(url_data).load();
                                });
                            }
                            else{
                                swal("Maaf!", "Data Gagal Dihapus!", "error");
                            }
                        }
                    });
                } else {     
                    swal("Dibatalkan", "Proses hapus data telah dibatakan", "error");   
                } 
            });
        });
    });
</script>