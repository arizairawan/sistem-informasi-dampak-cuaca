<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-8">
            <h3>
                <?=$info['judul_informasi']?> 
                <br>
                <small><?=$info['tgl_input']?> </small>
            </h3>
            <img src="<?=base_url()?>assets/image/informasi/<?=$info['image']?>" style="max-width:100%">
            <div>
                <?=$info['isi_informasi']?>
            </div>
        </div>
    </div>
</section>