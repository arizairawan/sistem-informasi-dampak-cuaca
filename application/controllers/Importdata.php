<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Importdata extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library("PHPExcel");
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('file');
        $this->load->model('m_global');
        $this->load->model('m_crud');
        $this->load->library('form_validation');
    }




    public function index()
    {
        $data['kota'] = $this->m_global->get_all('kota');
        $data['kecamatan'] = $this->m_global->get_all('kecamatan');

        $this->load->view('formimport',$data);
    }

    public function prosesimport(){

        $hitung = 0;
        $success = 0;

        //load library phpExcel
        $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload

        $config['upload_path'] = './assets/';                                // Buat folder dengan nama "fileExcel" di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file'))
            $this->upload->display_errors();

        $data = array('upload_data' => $this->upload->data());
        $file = $data['upload_data']['file_name'];
        $inputFileName = './assets/'.$file;

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);


            $data = array(                                                      // Sesuaikan sama nama kolom tabel di database
                "npsn_sekolah" => $rowData[0][0],
                "nama_sekolah"=> $rowData[0][1],
                "email"=> $rowData[0][2],
                "password" => md5($rowData[0][3]),
                "id_tingkat_sekolah"=> $this->input->post('id_tingkat_sekolah'),
                "id_upt"=> $this->input->post('id_upt'),
            );

            $insert = $this->db->insert("sekolah", $data);                   // Sesuaikan nama dengan nama tabel untuk melakukan 

            $dok = array(
                "id_tingkat_adiwiyata"=> $this->input->post('id_tingkat_adiwiyata'),
                'id_sekolah' => $rowData[0][0],
                'approve_dokumen' => 'Y',
                'verifikasi_dokumen' => 'Y',
                'validasi_dokumen' => 'Y',
                'tgl_verifikasi' => date('Y-m-d H:i:s'),
                'tgl_validasi' => date('Y-m-d H:i:s'),
                'id_user_approve' => 1,
                'id_user_verifikasi' => 4,
                'id_user_validasi' => 3,
            );
            $insert = $this->db->insert("dokumen", $dok);

            $hitung++;

            delete_files($file);                                  // menghapus semua file .xls yang diupload
        }

        $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i>
                    '.$hitung.' Data telah ditambahkan
                  </div>');

        redirect('importdata');
    }


    public function prosesimportrelawan(){

        $hitung = 0;
        $success = 0;

        //load library phpExcel
        $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload

        $config['upload_path'] = './assets/';                                // Buat folder dengan nama "fileExcel" di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file'))
            $this->upload->display_errors();

        $data = array('upload_data' => $this->upload->data());
        $file = $data['upload_data']['file_name'];
        $inputFileName = './assets/'.$file;

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);


            $data = array(  
                'nama_relawan' =>  $rowData[0][6],
                'no_ktp' =>  $rowData[0][0],
                'alamat' =>  $rowData[0][1],
                'telepon' =>  $rowData[0][4],
                'id_kecamatan' =>  $rowData[0][2],
                'email' =>  $rowData[0][5],
                'password' => md5($rowData[0][6]),
                'pas_foto' => 'image-pas.png',
                'foto_ktp' => 'image-ktp.png',

            );

            $insert = $this->db->insert("relawan", $data);                   // Sesuaikan nama dengan nama tabel untuk melakukan 

            $last_id = $this->db->insert_id();

            $hasil = array(
                'hasil_pelatihan' => 'image-hasil.png',
                'id_relawan' => $last_id,
                'status_verifikasi' => 'Y',
                'id_user_verifikasi' => 1,
                'tgl_verifikasi' => date('Y-m-d'),
                'status_validasi' => 'Y',
                'id_user_validasi' => 4,
                'tgl_validasi' => date('Y-m-d'),
            );

            $insert = $this->db->insert("hasil_pelatihan", $hasil); 

            $hitung++;

            delete_files($file);                                  // menghapus semua file .xls yang diupload
        }

        $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i>
                    '.$hitung.' Data telah ditambahkan
                  </div>');

        redirect('importdata');
    }

    public function prosesimportstasiun(){

        $hitung = 0;
        $success = 0;

        //load library phpExcel
        $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload

        $config['upload_path'] = './assets/';                                // Buat folder dengan nama "fileExcel" di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file'))
            $this->upload->display_errors();

        $data = array('upload_data' => $this->upload->data());
        $file = $data['upload_data']['file_name'];
        $inputFileName = './assets/'.$file;

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);


            $data = array(  
                'nama_stasiun' =>  $rowData[0][0],
                'alamat' =>  $rowData[0][1],
                'telepon' =>  $rowData[0][3],
                'id_kecamatan' =>  $rowData[0][2],
                'email' =>  $rowData[0][4],
                'password' => md5($rowData[0][5]),
            );

            $insert = $this->db->insert("stasiun", $data);                   // Sesuaikan nama dengan nama tabel untuk melakukan 


            $hitung++;

            delete_files($file);                                  // menghapus semua file .xls yang diupload
        }

        $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i>
                    '.$hitung.' Data telah ditambahkan
                  </div>');

        redirect('importdata');
    }


    public function prosesimportcurahhujan(){

        $hitung = 0;
        $success = 0;

        //load library phpExcel
        $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload

        $config['upload_path'] = './assets/';                                // Buat folder dengan nama "fileExcel" di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file'))
            $this->upload->display_errors();

        $data = array('upload_data' => $this->upload->data());
        $file = $data['upload_data']['file_name'];
        $inputFileName = './assets/'.$file;

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            $tgl = '2018-10-'.($row-1);

            for($x = 1; $x < count($rowData[0]); $x++){
                $data = array(  
                    'curahhujan' =>  $rowData[0][$x],
                    'lampiran' =>  'image-hasil.png',
                    'tgl_lapor' =>  date('Y-m-d',strtotime($tgl)),
                    'id_relawan' =>  $x,
                    'status_verifikasi' => 'Y',
                    'id_user_verifikasi' => 1,
                    'tgl_verifikasi' => date('Y-m-d'),
                    'status_validasi' => 'Y',
                    'id_user_validasi' => 4,
                    'tgl_validasi' => date('Y-m-d'),
                );
                $insert = $this->db->insert("data_curahhujan", $data); 
            }


            // Sesuaikan nama dengan nama tabel untuk melakukan 


            $hitung++;

            delete_files($file);                                  // menghapus semua file .xls yang diupload
        }

        $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i>
                    '.$hitung.' Data telah ditambahkan
                  </div>');

        redirect('importdata');
    }

    public function prosesimportkelembapan(){

        $hitung = 0;
        $success = 0;

        //load library phpExcel
        $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload

        $config['upload_path'] = './assets/';                                // Buat folder dengan nama "fileExcel" di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file'))
            $this->upload->display_errors();

        $data = array('upload_data' => $this->upload->data());
        $file = $data['upload_data']['file_name'];
        $inputFileName = './assets/'.$file;

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            $tgl = '2018-10-'.($row-1);

            for($x = 1; $x < count($rowData[0]); $x++){
                $data = array(
                    'kelembapan' =>  $rowData[0][$x],
                    'lampiran' =>  'image-hasil.png',
                    'tgl_lapor' =>  date('Y-m-d',strtotime($tgl)),
                    'id_stasiun' =>  $x+3,
                    'status_verifikasi' => 'Y',
                    'id_user_verifikasi' => 1,
                    'tgl_verifikasi' => date('Y-m-d'),
                    'status_validasi' => 'Y',
                    'id_user_validasi' => 4,
                    'tgl_validasi' => date('Y-m-d'),
                );
                $insert = $this->db->insert("data_kelembapan", $data); 
            }

            // Sesuaikan nama dengan nama tabel untuk melakukan 

            $hitung++;

            delete_files($file);                                  // menghapus semua file .xls yang diupload
        }

        $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i>
                    '.$hitung.' Data telah ditambahkan
                  </div>');

        redirect('importdata');
    }


    public function prosesimportsuhu(){

        $hitung = 0;
        $success = 0;

        //load library phpExcel
        $fileName = time() . $_FILES['file']['name'];                     // Sesuai dengan nama Tag Input/Upload

        $config['upload_path'] = './assets/';                                // Buat folder dengan nama "fileExcel" di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('file'))
            $this->upload->display_errors();

        $data = array('upload_data' => $this->upload->data());
        $file = $data['upload_data']['file_name'];
        $inputFileName = './assets/'.$file;

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

            $tgl = '2018-10-'.($row-1);

            for($x = 1; $x < count($rowData[0]); $x = $x + 3){
                $data = array(
                    'suhu_min' =>  $rowData[0][1],
                    'suhu_max' =>  $rowData[0][2],
                    'suhu_avg' =>  $rowData[0][3],
                    'lampiran' =>  'image-hasil.png',
                    'tgl_lapor' =>  date('Y-m-d',strtotime($tgl)),
                    'id_stasiun' =>  $x+3,
                    'status_verifikasi' => 'Y',
                    'id_user_verifikasi' => 1,
                    'tgl_verifikasi' => date('Y-m-d'),
                    'status_validasi' => 'Y',
                    'id_user_validasi' => 4,
                    'tgl_validasi' => date('Y-m-d'),
                );
                $insert = $this->db->insert("data_suhu", $data); 
            }

            // Sesuaikan nama dengan nama tabel untuk melakukan 

            $hitung++;

            delete_files($file);                                  // menghapus semua file .xls yang diupload
        }

        $this->session->set_flashdata('message', '<div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon fa fa-check"></i>
                    '.$hitung.' Data telah ditambahkan
                  </div>');

        redirect('importdata');
    }


    public function generatecurahhujan($tipe, $id, $start_date, $end_date)
    {
        $namaid = 'id_'.$tipe;
        $no = 0;
        while (strtotime($start_date) <= strtotime($end_date)) {

            $data = array(
                $namaid => $id,
                'tgl_lapor' => $start_date,
                'curahhujan' => mt_rand(0, 120),
                'lampiran' => 'image-hasil.png',
                'status_verifikasi' => 'Y',
                'id_user_verifikasi' => 1,
                'tgl_verifikasi' => date('Y-m-d'),
                'status_validasi' => 'Y',
                'id_user_validasi' => 4,
                'tgl_validasi' => date('Y-m-d')
            );

            $this->db->where('tgl_lapor', $start_date);
            $this->db->where($namaid, $id);
            $cek = $this->db->get('data_curahhujan');

            if($cek->num_rows() == 0){
                $this->m_crud->insert('data_curahhujan',$data);
                $no++;
            }else{
                echo"data tgl $start_date sudah ada <br>";
            }



            $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));

        }
        echo "Total generate : ".$no." data";
    }
    
    public function generatecurahhujanloop($tipe, $idstart, $idend, $start_date, $end_date)
    {
        $namaid = 'id_'.$tipe;
        $no = 0;
        for($i = $idstart; $i <= $idend; $i++)
        {
            while (strtotime($start_date) <= strtotime($end_date)) {

                $data = array(
                    $namaid => $i,
                    'tgl_lapor' => $start_date,
                    'curahhujan' => mt_rand(0, 120),
                    'lampiran' => 'image-hasil.png',
                    'status_verifikasi' => 'Y',
                    'id_user_verifikasi' => 1,
                    'tgl_verifikasi' => date('Y-m-d'),
                    'status_validasi' => 'Y',
                    'id_user_validasi' => 4,
                    'tgl_validasi' => date('Y-m-d')
                );

                $this->db->where('tgl_lapor', $start_date);
                $this->db->where($namaid, $i);
                $cek = $this->db->get('data_curahhujan');

                if($cek->num_rows() == 0){
                    $this->m_crud->insert('data_curahhujan',$data);
                    $no++;
                }else{
                    echo"data tgl $start_date sudah ada <br>";
                }



                $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));

            }
            echo "Total generate : ".$no." data";
        }
        
    }
}