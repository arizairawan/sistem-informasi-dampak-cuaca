<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {
    public $title = 'Kecamatan';
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
    }
	public function index()
	{
        $data = array(
            'nav' => 'kecamatan',
            'title' => $this->title,
            'views' => 'kecamatan_table',
            'kota' => $this->m_global->get_all('kota')
        );
		$this->load->view('starter',$data);
	}
    public function loaddata($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('kecamatan','id_kecamatan','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id('kecamatan','id_kecamatan',$ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($proyek as $pry):
        
        $kota = $this->m_global->get_by_id('kota','id_kota', $pry['id_kota']);

        $arra = array(
            'no' => $no++,
            'id_kecamatan' => simple_encrypt($pry['id_kecamatan']),
            'nama_kecamatan' => $pry['nama_kecamatan'],
            'id_kota' => $kota['id_kota'],
            'nama_kota' => $kota['nama_kota'],
            'id_spasial' => $pry['id_spasial'],
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function addkecamatan()
    {
        $data = array(
            'nama_kecamatan' => $this->input->post('nama_kecamatan'),
            'id_kota' => $this->input->post('id_kota'),
            'id_spasial' => $this->input->post('id_spasial'),
        );
        $insert = $this->m_crud->insert('kecamatan',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function editkecamatan()
    {
        $id = simple_decrypt($this->input->post('id_kecamatan'));
        $data = array(
            'nama_kecamatan' => $this->input->post('nama_kecamatan'),
            'id_kota' => $this->input->post('id_kota'),
            'id_spasial' => $this->input->post('id_spasial'),
        );
        $insert = $this->m_crud->update('kecamatan','id_kecamatan',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function hapus()
    {
        $id = simple_decrypt($this->input->post('id'));
        
        $delete = $this->m_crud->delete('kecamatan','id_kecamatan',$id);

        if($delete == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
}
