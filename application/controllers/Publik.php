<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publik extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
	public function index()
	{
        $data = array(
            'nav' => 'home',
            'title' => 'Home',
            'views' => 'home'
        );
		$this->load->view('publik',$data);
	}
    
    public function relawan()
	{
        $data = array(
            'nav' => 'relawan',
            'title' => 'relawan',
            'views' => 'publik_relawan',
            'upt' => $this->m_global->get_all('upt'),
            'tingkat_adiwiyata' => $this->m_global->get_all('tingkat_adiwiyata'),
            'tingkat_relawan' => $this->m_global->get_all('tingkat_relawan'),
        );
		$this->load->view('publik',$data);
	}
    public function rute($lat, $lng)
	{
        $data = array(
            'nav' => 'Rute',
            'title' => 'Rute',
            'x' => $lat,
            'y' => $lng,
            'views' => 'publik_spasial_rute'
        );
		$this->load->view('publik',$data);
	}
    public function spasial()
	{
        $data = array(
            'nav' => 'spasial',
            'title' => 'Spasial',
            'kecamatan' => $this->m_global->get_all('kecamatan'),
            'kota' => $this->m_global->get_all('kota'),
            'views' => 'publik_spasial'
        );
		$this->load->view('publik',$data);
	}
    
    public function detail($id = false)
    {
        if($id === false)
        {
            show_404();
        }
        
        $ids = simple_decrypt($id);
        $relawan = $this->m_adiwiyata->get_relawan($ids);
        
        $data = array(
            'nav' => 'relawan',
            'title' => $relawan[0]['nama_relawan'],
            'views' => 'publik_detail',
            'relawan' => $relawan[0]
        );
		$this->load->view('publik',$data);
    }
    
    public function detail2($tipe, $id = false)
    {
        if($id === false)
        {
            show_404();
        }
        
        if($tipe == 'relawan'){
            $relawan = $this->m_global->get_by_id('relawan','id_relawan',$id);
            $nama = $relawan['nama_relawan'];
        }else{
            $relawan = $this->m_global->get_by_id('stasiun','id_stasiun',$id);
            $nama = $relawan['nama_stasiun'];
        }
        
        
        $curahhujan = $this->m_global->get_list_by_id('data_curahhujan','id_relawan',$id);
        
        
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$relawan['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$kec['id_kota']);
        
        $data = array(
            'nav' => 'relawan',
            'title' => $nama,
            'views' => 'publik_detail',
            'relawan' => $relawan,
            'kecamatan' => $kec,
            'kota' => $kota,
            'id' => $id,
            'tipe' => $tipe
        );
		$this->load->view('publik_detail2',$data);
    }
    public function getdatagrafik($datanya, $tglawal, $tglakhir, $tipe, $id)
    {
        if($tipe == 'relawan')
        {
            $request = $this->db->query("select *,DATE_FORMAT(tgl_lapor,'%Y-%m-%d') as tglx from $datanya where DATE_FORMAT(tgl_lapor,'%Y-%m-%d') between '$tglawal' and '$tglakhir' and id_relawan = '$id'")->result_array();
        }
        else
        {
            $request = $this->db->query("select *,DATE_FORMAT(tgl_lapor,'%Y-%m-%d') as tglx from $datanya where DATE_FORMAT(tgl_lapor,'%Y-%m-%d') between '$tglawal' and '$tglakhir' and id_stasiun = '$id'")->result_array();
        }

        $no = 1;
        $arr = array();
        foreach($request as $pry):
        
        $arra = array(
            'tgl_lapor' => $pry['tglx'],
            'curahhujan' => $pry['curahhujan']
        );
        

        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function readinfo($id = false)
    {
        if($id === false)
        {
            show_404();
        }
        
        $ids = simple_decrypt($id);
        $info = $this->m_global->get_one_by_id('informasi','id_informasi',$ids);
        
        $data = array(
            'nav' => 'informasi',
            'title' => $info['judul_informasi'],
            'views' => 'publik_read',
            'info' => $info
        );
		$this->load->view('publik',$data);
    }
    
    public function informasi()
	{
        $data = array(
            'nav' => 'informasi',
            'title' => 'Informasi',
            'views' => 'publik_informasi'
        );
		$this->load->view('publik',$data);
	}
    
    public function citra($id)
	{
        $ids = simple_decrypt($id);
        $info = $this->m_global->get_one_by_id('relawan','npsn_relawan',$ids);
        $data = array(
            'nav' => 'relawan',
            'title' => 'Citra Adiwiyata '.$info['nama_relawan'],
            'id' => $id,
            'views' => 'publik_citra'
        );
		$this->load->view('publik',$data);
	}
    
    
    
    
}
