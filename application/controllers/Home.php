<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
    public $title = 'Home';
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
    }
	public function index()
	{
        $data = array(
            'nav' => 'home',
            'title' => $this->title,
            'views' => 'home',
            'jml' => $this->hitung()
        );
		$this->load->view('starter',$data);
	}
    
    function hitung()
    {
        $tgl = date('Y-m-d');
        $bln = date('Y-m');
        $id = $_SESSION['id_user'];
//        $d=cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        $d = date('d');
        $getlaporan = $this->db->query("SELECT * FROM `data_curahhujan` WHERE date_format(tgl_lapor, '%Y-%m') = '$bln' and id_relawan = '$id' and tgl_lapor <= '$tgl'")->num_rows();
        
        $data = array(
            'jml_hari' => $d,
            'jml_lapor' => $getlaporan,
            'jml_bolos' => $d - $getlaporan
        );
        return $data;
    }
    
}
