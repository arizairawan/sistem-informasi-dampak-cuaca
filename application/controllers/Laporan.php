<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
    public $title = 'Laporan';
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
    }
	public function index()
	{
        $data = array(
            'nav' => 'laporan',
            'title' => $this->title,
            'views' => 'laporan_view',
            'relawan' => $this->m_global->num_by_id('hasil_pelatihan','status_validasi', 'Y'),
            'stasiun' => $this->m_global->num('stasiun'),
            'request' => $this->m_global->num('request'),
        );
		$this->load->view('starter',$data);
	}
    public function loaddata($tahun)
    {
        
        $arrayall = array();
        for($bln = 1; $bln <= 12; $bln++){
            
            $bln = sprintf("%02d", $bln);
            $this->db->select("SUM(curahhujan) as rata,  date_format(tgl_lapor, '%m') as bulan");
            $this->db->where("date_format(tgl_lapor, '%Y') = '$tahun'");
            $this->db->where("date_format(tgl_lapor, '%m') = '$bln'");
            $proyek = $this->db->get('data_curahhujan')->row_array();
            
            $arr = array(
                'x' => $tahun.'-'.$bln,
                'rata' => round($proyek['rata'] /  12)
            );
            array_push($arrayall, $arr);
        }
        
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
   
}
