<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relawan extends CI_Controller {
    public $title = 'Relawan';
    public function __construct() {
        parent::__construct();
        
    }
	public function index()
	{
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
        $data = array(
            'nav' => 'relawan',
            'title' => $this->title,
            'views' => 'relawan_table',
            'kecamatan' => $this->m_global->get_all('kecamatan'),
            'kota' => $this->m_global->get_all('kota'),
        );
		$this->load->view('starter',$data);
	}
    
    function hitung($id, $tglvalid, $statusvalid)
    {
        $blnvalid = date('Y-m', strtotime($tglvalid));
        $dvalid = date('d', strtotime($tglvalid));
        
        $tgl = date('Y-m-d');
        $bln = date('Y-m');
//        $d=cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        if($blnvalid == $bln && $statusvalid = 'Y'){
            $d = date('d') - $dvalid;
        }else if($statusvalid == 'N'){
            $d = 0;
        }else{
            $d = date('d');
        }
        
        $getlaporan = $this->db->query("SELECT * FROM `data_curahhujan` WHERE date_format(tgl_lapor, '%Y-%m') = '$bln' and id_relawan = '$id' and tgl_lapor <= '$tgl'")->num_rows();
        
        $data = array(
            'jml_hari' => $d,
            'jml_lapor' => $getlaporan,
            'jml_bolos' => $d - $getlaporan
        );
        return $data;
    }
    
    public function loaddata($id = false)
    {   
        if($id === false)
        {
            $relawan = $this->m_global->get_all_order('relawan', 'id_relawan', 'desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $relawan = $this->m_global->get_list_by_id('relawan', 'id_relawan', $ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($relawan as $pry):
        
        
        //kecamatan
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan', $pry['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota', $kec['id_kota']);
        
        $hasil = $this->m_global->get_by_id('hasil_pelatihan','id_relawan',$pry['id_relawan']);
        
        
        $ver = $this->m_global->get_by_id('user','id_user',$hasil['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$hasil['id_user_validasi']);
        
        $arra = array(
            'no' => $no++,
            'id_relawan' => simple_encrypt($pry['id_relawan']),
            'nama_relawan' => $pry['nama_relawan'],
            'no_ktp' => $pry['no_ktp'],
            'foto_ktp' => site_url('assets/ktp/'.$pry['foto_ktp']),
            'pas_foto' => site_url('assets/foto/'.$pry['pas_foto']),
            'alamat' => $pry['alamat'],
            'telepon' => $pry['telepon'],
            'email' => $pry['email'],
            'id_kecamatan' => $pry['id_kecamatan'],
            'nama_kecamatan' => $kec['nama_kecamatan'],
            'id_kota' => $kec['id_kota'],
            'nama_kota' => $kota['nama_kota'],
            'tgl_daftar' => $pry['tgl_daftar'],
            'status_verifikasi' => $hasil['status_verifikasi'],
            'tgl_verifikasi' => $hasil['tgl_verifikasi'],
            'status_validasi' => $hasil['status_validasi'],
            'tgl_validasi' => $hasil['tgl_validasi'],
            'suspend_relawan' => $pry['suspend_relawan'],
            'tgl_suspend' => $pry['tgl_suspend'],
            'status_vr' => cekstatus($hasil['status_verifikasi'], $hasil['tgl_verifikasi'],$ver['nama_user']),
            'status_vl' => cekstatus($hasil['status_validasi'], $hasil['tgl_validasi'],$val['nama_user']),
            'jml' => $this->hitung($pry['id_relawan'], $hasil['tgl_validasi'], $hasil['status_validasi'])
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function loaddatax($id = false)
    {   
        $pry = $this->m_global->get_by_id('relawan', 'id_relawan', $id);
        //kecamatan
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan', $pry['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota', $kec['id_kota']);
        
        $hasil = $this->m_global->get_by_id('hasil_pelatihan','id_relawan',$pry['id_relawan']);
        
        
        $ver = $this->m_global->get_by_id('user','id_user',$hasil['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$hasil['id_user_validasi']);
        
        $arra = array(
            'id_relawan' => simple_encrypt($pry['id_relawan']),
            'nama_relawan' => $pry['nama_relawan'],
            'no_ktp' => $pry['no_ktp'],
            'foto_ktp' => site_url('assets/ktp/'.$pry['foto_ktp']),
            'pas_foto' => site_url('assets/foto/'.$pry['pas_foto']),
            'alamat' => $pry['alamat'],
            'telepon' => $pry['telepon'],
            'email' => $pry['email'],
            'id_kecamatan' => $pry['id_kecamatan'],
            'nama_kecamatan' => $kec['nama_kecamatan'],
            'id_kota' => $kec['id_kota'],
            'nama_kota' => $kota['nama_kota'],
            'tgl_daftar' => $pry['tgl_daftar'],
            'status_verifikasi' => $hasil['status_verifikasi'],
            'tgl_verifikasi' => $hasil['tgl_verifikasi'],
            'status_validasi' => $hasil['status_validasi'],
            'tgl_validasi' => $hasil['tgl_validasi'],
            'status_vr' => cekstatus($hasil['status_verifikasi'], $hasil['tgl_verifikasi'],$ver['nama_user']),
            'status_vl' => cekstatus($hasil['status_validasi'], $hasil['tgl_validasi'],$val['nama_user']),
        );


        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arra, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    
    public function detail($id = false)
    {
        if($id === false)
        {
            show_404();
        }
        
        $ids = simple_decrypt($id);
        $relawan = $this->m_global->get_by_id('relawan','id_relawan',$ids);
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$relawan['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$kec['id_kota']);
        
        $hasil = $this->m_global->get_by_id('hasil_pelatihan','id_relawan',$ids);
        
        $approve = $this->m_global->get_by_id('user','id_user',$relawan['id_user_approve']);
        $ver = $this->m_global->get_by_id('user','id_user',$hasil['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$hasil['id_user_validasi']);
        
        if($relawan['suspend_relawan'] == 'Y')
        {
            $susppp = "<b>Ditangguhkan : </b>".cekstatus($relawan['suspend_relawan'],$relawan['tgl_suspend'], 'Staff')." <button class='btn btn-warning btn-xs' id='aktifkan' data-id='$id'>Aktifkan Akun</button>";
        }else{
            $susppp = "";
        }
        $data = array(
            'nav' => 'relawan',
            'title' => $this->title." - ".$relawan['nama_relawan'],
            'views' => 'relawan_detail',
            'relawan' => $relawan,
            'kec' => $kec,
            'kota' => $kota,
            'hasil'=> $hasil,
            'approvex' => cekstatus($relawan['approve_pendaftaran'], $relawan['tgl_approve'], $approve['nama_user']),
            'verifikasix' => cekstatus($hasil['status_verifikasi'],$hasil['tgl_verifikasi'], $ver['nama_user']),
            'validasix' => cekstatus($hasil['status_validasi'],$hasil['tgl_validasi'], $val['nama_user']),
            'suspend' => $susppp,
        );
		$this->load->view('starter',$data);
    }
    
    
    public function uploadhasilpelatihan()
    {
        $var = 'hasil_pelatihan';
        $dir = './assets/hasil_pelatihan/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        if ( $upload1 == "")
        {
            echo "error";
        }
        else
        {
            $data = array(
                'hasil_pelatihan' => $upload1['file_name'],
                'id_relawan' => $_SESSION['id_user'],
            );
            
            $cek = $this->m_global->num_by_id('hasil_pelatihan', 'id_relawan', $_SESSION['id_user']);
            if($cek != 0)
            {
                $this->m_crud->update('hasil_pelatihan', 'id_relawan', $data, $_SESSION['id_user']);
            }
            else{
                $this->m_crud->insert('hasil_pelatihan',$data);
            }
            
            echo"success";
        }
    }
    
    public function editprofil()
    {
        $relawan = $this->m_global->get_by_id('relawan','id_relawan', $_SESSION['id_user']);
        
        $data = array(
            'nav' => 'relawan',
            'title' => $this->title." ".$relawan['nama_relawan'],
            'views' => 'relawan_edit',
            'relawan' => $relawan,
            'kecamatan' => $this->m_global->get_all('kecamatan'),
            'kota' => $this->m_global->get_all('kota'),
        );
		$this->load->view('starter',$data);
    }
    
    public function updateprofil()
    {
        $var = 'foto_ktp';
        $dir = './assets/ktp/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        $var = 'pas_foto';
        $dir = './assets/foto/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload2 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        $data = array(
            'nama_relawan' => $this->input->post('nama_relawan'),
            'no_ktp' => $this->input->post('no_ktp'),
            'alamat' => $this->input->post('alamat'),
            'telepon' => $this->input->post('telepon'),
            'id_kecamatan' => $this->input->post('id_kecamatan'),
            'email' => $this->input->post('email'),
        );
        
        if($upload1 != ""){
            $data['pas_foto'] = $upload1['file_name'];
        }
        if($upload2 != ""){
            $data['foto_ktp'] = $upload2['file_name'];
        }
        
        if($this->input->post('password1') != ''){
            $data['password'] = md5($this->input->post('password1'));
        }
        
        if($this->input->post('password1') == $this->input->post('password2')){
            $this->m_crud->update('relawan','id_relawan',$data,$_SESSION['id_user']);
            echo"success";
        }else{
            echo"password";
        }
        
    }

    public function approve_pendaftaran()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'approve_pendaftaran' => $this->input->post('status'),
            'tgl_approve' => date('Y-m-d'),
            'id_user_approve' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('relawan','id_relawan',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function verifikasi()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_verifikasi' => $this->input->post('status'),
            'tgl_verifikasi' => date('Y-m-d'),
            'id_user_verifikasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('hasil_pelatihan','id_relawan',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    public function validasi()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_validasi' => $this->input->post('status'),
            'tgl_validasi' => date('Y-m-d'),
            'id_user_validasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('hasil_pelatihan','id_relawan',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function aktifkanakun()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'suspend_relawan' => 'N',
        );
        $insert = $this->m_crud->update('relawan','id_relawan',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function updateprofile()
    {
        $data = array(
            'nama_relawan' => $this->input->post('nama_relawan'),
            'id_relawan' => $this->input->post('id_relawan'),
            'alamat' => $this->input->post('alamat'),
            'telepon' => $this->input->post('telepon'),
            'kode_pos' => $this->input->post('kode_pos'),
            'id_upt' => $this->input->post('id_upt'),
            'id_tingkat_relawan' => $this->input->post('id_tingkat_relawan'),
            'email' => $this->input->post('email'),
        );
        
        $var = 'sk_akreditasi';
        $dir = './assets/sk/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        $var = 'sk_pernyataan_kepsek';
        $dir = './assets/sk/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload2 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        $var = 'sk_kurikulum';
        $dir = './assets/sk/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload3 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        if ( $upload1 != "")
        {
            $data['sk_akreditasi'] = $upload1['file_name'];
        }
        
        if ($upload2 != "")
        {
            $data['sk_pernyataan_kepsek'] = $upload2['file_name'];
        }
        
        if ($upload3 != "")
        {
            $data['sk_kurikulum'] = $upload3['file_name'];
        }
       
        if($this->input->post('password') != "")
        {
            $data['password'] = md5($this->input->post('password'));
        }

        $this->m_crud->update('relawan','id_relawan',$data,$_SESSION['id_user']);
        
        redirect('relawan/editprofil/'.simple_encrypt($_SESSION['id_user']));


    }
}
