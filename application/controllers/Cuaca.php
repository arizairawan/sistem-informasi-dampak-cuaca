<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuaca extends CI_Controller {
    public $title = 'Cuaca';
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
    }
	public function index()
	{
        $data = array(
            'nav' => 'cuaca',
            'title' => "Curah hujan",
            'views' => 'cuaca'
        );
		$this->load->view('starter',$data);
	}
    public function kelembapan()
	{
        $data = array(
            'nav' => 'kelembapan',
            'title' => "Kelembapan",
            'views' => 'kelembapan'
        );
		$this->load->view('starter',$data);
	}
    public function suhu()
	{
        $data = array(
            'nav' => 'suhu',
            'title' => "Suhu",
            'views' => 'suhu'
        );
		$this->load->view('starter',$data);
	}
     
    public function loaddatacurahhujan_bydate($tgl = false)
    {
        $tglhariini = date('Y-m-d');
        if($tgl === false)
        {
            $tgl = $tglhariini;
        }
        
        $this->db->join('hasil_pelatihan', 'hasil_pelatihan.id_relawan = relawan.id_relawan', 'left');
        $this->db->where('hasil_pelatihan.status_validasi', 'Y');
        $relawan = $this->db->get('relawan')->result_array();
        
        $no = 1;
        $arr = array();
        foreach($relawan as $pry):
        
        //kecamatan
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan', $pry['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota', $kec['id_kota']);
        
        
        $this->db->where("date_format(tgl_lapor, '%Y-%m-%d') = '$tgl'");
        $this->db->where('id_relawan', $pry['id_relawan']);
        $getlapor = $this->db->get('data_curahhujan')->row_array();
        
        

        if($getlapor['curahhujan'] != null){
            //cek standar
            $cekstandar = $this->m_global->cek_standar($getlapor['curahhujan']);
            $ckst = 'Tidak Hujan';
            if($cekstandar['nama_kategori'] != null){
                $ckst = $cekstandar['nama_kategori'];
            }
            
            $curahhujan = $getlapor['curahhujan'];
        }else{
            $curahhujan = '-';
            $ckst = '-';
            if($tgl != $tglhariini)
            {
                $ckst = '<b class="text-danger">Tidak lapor</b>';
            }
        }
        
        
        
        
        $ver = $this->m_global->get_by_id('user','id_user',$getlapor['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$getlapor['id_user_validasi']);
        
        $arra = array(
            'no' => $no++,
            'id_curahhujan' => simple_encrypt($getlapor['id_curahhujan']),
            'id_relawan' => simple_encrypt($pry['id_relawan']),
            'nama_relawan' => $pry['nama_relawan'],
            'id_kecamatan' => $pry['id_kecamatan'],
            'nama_kecamatan' => $kec['nama_kecamatan'],
            'id_kota' => $kec['id_kota'],
            'nama_kota' => $kota['nama_kota'],
            'curahhujan' => $curahhujan,
            'standar' => $ckst,
            'lampiran' => $getlapor['lampiran'],
            'tgl_lapor' => $getlapor['tgl_lapor'],
            'status_verifikasi' => $getlapor['status_verifikasi'],
            'tgl_verifikasi' => $getlapor['tgl_verifikasi'],
            'status_validasi' => $getlapor['status_validasi'],
            'tgl_validasi' => $getlapor['tgl_validasi'],
            'verifikasi' => cekstatus($getlapor['status_verifikasi'],$getlapor['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($getlapor['status_validasi'],$getlapor['tgl_validasi'], $val['nama_user']),
        );


        array_push($arr,$arra);
        endforeach;
        
        
        $stasiun = $this->db->get('stasiun')->result_array();
        foreach($stasiun as $pry):
        
        //kecamatan
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan', $pry['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota', $kec['id_kota']);
        
        $this->db->where("date_format(tgl_lapor, '%Y-%m-%d') = '$tgl'");
        $this->db->where('id_stasiun', $pry['id_stasiun']);
        $getlapor = $this->db->get('data_curahhujan')->row_array();
        
        

        if($getlapor['curahhujan'] != null){
            //cek standar
            $cekstandar = $this->m_global->cek_standar($getlapor['curahhujan']);
            $ckst = 'Tidak Hujan';
            if($cekstandar['nama_kategori'] != null){
                $ckst = $cekstandar['nama_kategori'];
            }
            
            $curahhujan = $getlapor['curahhujan'];
        }else{
            $curahhujan = '-';
            $ckst = '-';
            if($tgl != $tglhariini)
            {
                $ckst = '<b class="text-danger">Tidak lapor</b>';
            }
        }
        
        
        
        
        $ver = $this->m_global->get_by_id('user','id_user',$getlapor['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$getlapor['id_user_validasi']);
        
        $arra = array(
            'no' => $no++,
            'id_curahhujan' => simple_encrypt($getlapor['id_curahhujan']),
            'id_relawan' => simple_encrypt($pry['id_stasiun']),
            'nama_relawan' => 'Stasiun '.$pry['nama_stasiun'],
            'id_kecamatan' => $pry['id_kecamatan'],
            'nama_kecamatan' => $kec['nama_kecamatan'],
            'id_kota' => $kec['id_kota'],
            'nama_kota' => $kota['nama_kota'],
            'curahhujan' => $curahhujan,
            'standar' => $ckst,
            'lampiran' => $getlapor['lampiran'],
            'tgl_lapor' => $getlapor['tgl_lapor'],
            'status_verifikasi' => $getlapor['status_verifikasi'],
            'tgl_verifikasi' => $getlapor['tgl_verifikasi'],
            'status_validasi' => $getlapor['status_validasi'],
            'tgl_validasi' => $getlapor['tgl_validasi'],
            'verifikasi' => cekstatus($getlapor['status_verifikasi'],$getlapor['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($getlapor['status_validasi'],$getlapor['tgl_validasi'], $val['nama_user']),
        );


        array_push($arr,$arra);
        endforeach;
        
        
        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function loaddatacurahhujan($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('data_curahhujan','id_curahhujan','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id_order('data_curahhujan', 'id_relawan', $ids, 'id_curahhujan', 'desc');
        }
        
        
        $no = 1;
        $arr = array();
        foreach($proyek as $pry):
        
        if($pry['id_relawan'] != 0){
            $relawan = $this->m_global->get_by_id('relawan','id_relawan',$pry['id_relawan']);
            $nama = $relawan['nama_relawan'];
        }else{
            $relawan = $this->m_global->get_by_id('stasiun','id_stasiun',$pry['id_stasiun']);
            $nama = $relawan['nama_stasiun'];
        }
        
        
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$relawan['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$kec['id_kota']);
        
        $ver = $this->m_global->get_by_id('user','id_user',$pry['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$pry['id_user_validasi']);
        
        $cekstandar = $this->m_global->cek_standar($pry['curahhujan']);
        
        $ckst = 'Tidak Hujan';
        if($cekstandar['nama_kategori'] != null){
            $ckst = $cekstandar['nama_kategori'];
        }
        
        $arra = array(
            'no' => $no++,
            'id_curahhujan' => simple_encrypt($pry['id_curahhujan']),
            'curahhujan' => $pry['curahhujan'],
            'standar' => $ckst,
            'lampiran' => $pry['lampiran'],
            'tgl_lapor' => $pry['tgl_lapor'],
            'status_verifikasi' => $pry['status_verifikasi'],
            'tgl_verifikasi' => $pry['tgl_verifikasi'],
            'status_validasi' => $pry['status_validasi'],
            'tgl_validasi' => $pry['tgl_validasi'],
            'verifikasi' => cekstatus($pry['status_verifikasi'],$pry['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($pry['status_validasi'],$pry['tgl_validasi'], $val['nama_user']),
            'id_relawan' => simple_encrypt($pry['id_relawan']),
            'nama_relawan' => $nama,
            'nama_kecamatan' => $kec['nama_kecamatan'],
            'nama_kota' => $kota['nama_kota'],
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );
        
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function loaddatakelembapan($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('data_kelembapan','id_kelembapan','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id('data_kelembapan','id_stasiun',$ids);
        }
        
        $no = 1;
        $arr = array();
        foreach($proyek as $pry):
        
        $stasiun = $this->m_global->get_by_id('stasiun','id_stasiun',$pry['id_stasiun']);
        
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$stasiun['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$kec['id_kota']);
        
        $ver = $this->m_global->get_by_id('user','id_user',$pry['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$pry['id_user_validasi']);
        
        $arra = array(
            'no' => $no++,
            'id_kelembapan' => simple_encrypt($pry['id_kelembapan']),
            'kelembapan' => $pry['kelembapan'],
            'lampiran' => $pry['lampiran'],
            'tgl_lapor' => $pry['tgl_lapor'],
            'status_verifikasi' => $pry['status_verifikasi'],
            'tgl_verifikasi' => $pry['tgl_verifikasi'],
            'status_validasi' => $pry['status_validasi'],
            'tgl_validasi' => $pry['tgl_validasi'],
            'verifikasi' => cekstatus($pry['status_verifikasi'],$pry['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($pry['status_validasi'],$pry['tgl_validasi'], $val['nama_user']),
            'id_stasiun' => simple_encrypt($pry['id_stasiun']),
            'nama_stasiun' => $stasiun['nama_stasiun'],
            'nama_kecamatan' => $kec['nama_kecamatan'],
            'nama_kota' => $kota['nama_kota'],
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );
        
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    public function loaddatasuhu($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('data_suhu','id_suhu','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id('data_suhu','id_stasiun',$ids);
        }
        
        $no = 1;
        $arr = array();
        foreach($proyek as $pry):
        
        $stasiun = $this->m_global->get_by_id('stasiun','id_stasiun',$pry['id_stasiun']);
        
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$stasiun['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$kec['id_kota']);
        
        $ver = $this->m_global->get_by_id('user','id_user',$pry['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$pry['id_user_validasi']);
        
        $arra = array(
            'no' => $no++,
            'id_suhu' => simple_encrypt($pry['id_suhu']),
            'suhu_min' => $pry['suhu_min'],
            'suhu_max' => $pry['suhu_max'],
            'suhu_avg' => $pry['suhu_avg'],
            'lampiran' => $pry['lampiran'],
            'tgl_lapor' => $pry['tgl_lapor'],
            'status_verifikasi' => $pry['status_verifikasi'],
            'tgl_verifikasi' => $pry['tgl_verifikasi'],
            'status_validasi' => $pry['status_validasi'],
            'tgl_validasi' => $pry['tgl_validasi'],
            'verifikasi' => cekstatus($pry['status_verifikasi'],$pry['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($pry['status_validasi'],$pry['tgl_validasi'], $val['nama_user']),
            'id_stasiun' => simple_encrypt($pry['id_stasiun']),
            'nama_stasiun' => $stasiun['nama_stasiun'],
            'nama_kecamatan' => $kec['nama_kecamatan'],
            'nama_kota' => $kota['nama_kota'],
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );
        
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    
    public function verifikasi()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_verifikasi' => 'Y',
            'tgl_verifikasi' => date('Y-m-d'),
            'id_user_verifikasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('data_curahhujan','id_curahhujan',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    public function verifikasi2()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_verifikasi' => 'Y',
            'tgl_verifikasi' => date('Y-m-d'),
            'id_user_verifikasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('data_kelembapan','id_kelembapan',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    public function verifikasi3()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_verifikasi' => 'Y',
            'tgl_verifikasi' => date('Y-m-d'),
            'id_user_verifikasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('data_suhu','id_suhu',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    
    public function validasi()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_validasi' => 'Y',
            'tgl_validasi' => date('Y-m-d'),
            'id_user_validasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('data_curahhujan','id_curahhujan',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    public function validasi2()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_validasi' => 'Y',
            'tgl_validasi' => date('Y-m-d'),
            'id_user_validasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('data_kelembapan','id_kelembapan',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    public function validasi3()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_validasi' => 'Y',
            'tgl_validasi' => date('Y-m-d'),
            'id_user_validasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('data_suhu','id_suhu',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
}