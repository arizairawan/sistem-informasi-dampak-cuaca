<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
    public $title = 'Kategori';
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
    }
	public function index()
	{
        $data = array(
            'nav' => 'kategori',
            'title' => $this->title,
            'views' => 'kategori_table'
        );
		$this->load->view('starter',$data);
	}
    public function loaddata($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('data_kategori','id','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id('data_kategori','id',$ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($proyek as $pry):

        if($pry['standar_max'] == 0){
            $max = '-';
        }else{
            $max = $pry['standar_max'];
        }
        
        $arra = array(
            'no' => $no++,
            'id' => simple_encrypt($pry['id']),
            'min' => $pry['standar_min'],
            'max' => $max,
            'nama_kategori' => $pry['nama_kategori'],
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function addkategori()
    {
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
            'standar_min' => $this->input->post('standar_min'),
            'standar_max' => $this->input->post('standar_max'),
        );
        $insert = $this->m_crud->insert('data_kategori',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function editkategori()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
            'standar_min' => $this->input->post('standar_min'),
            'standar_max' => $this->input->post('standar_max'),
        );
        $insert = $this->m_crud->update('data_kategori','id',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function hapus()
    {
        $id = simple_decrypt($this->input->post('id'));
        
        $delete = $this->m_crud->delete('data_kategori','id',$id);

        if($delete == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
}
