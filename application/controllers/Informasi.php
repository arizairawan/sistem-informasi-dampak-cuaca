<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class informasi extends CI_Controller {
    public $title = 'Informasi';
    public function __construct() {
        parent::__construct();
        
    }
	public function index()
	{
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
        $data = array(
            'nav' => 'informasi',
            'title' => $this->title,
            'views' => 'informasi_table'
        );
		$this->load->view('starter',$data);
	}
    public function loaddata($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('informasi','id_informasi','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id('informasi','id_informasi',$ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($proyek as $pry):

        $image = $pry['image'];
        if($pry['image'] == ''){
            $image = 'no_image.jpg';
        }
        
        $arra = array(
            'no' => $no++,
            'id_informasi' => simple_encrypt($pry['id_informasi']),
            'judul_informasi' => $pry['judul_informasi'],
            'isi_informasi' => $pry['isi_informasi'],
            'image_name' => $image,
            'image_dir' => base_url().'assets/image/informasi/'.$image,
            'tgl_input' => $pry['tgl_input'],
            'tgl_update' => $pry['tgl_update'],
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function addinformasi()
    {
        $var = 'image';
        $dir = './assets/image/informasi/';
        $all = 'jpg|png|gif|jpeg|ico';
        $upload = $this->m_crud->uploadfile01($var,$dir,$all);
        
        if ( $upload == "")
        {
            $data = array(
                'judul_informasi' => $this->input->post('judul_informasi'),
                'isi_informasi' => $this->input->post('isi_informasi'),
            );
        }
        else
        {
            $data = array(
                'judul_informasi' => $this->input->post('judul_informasi'),
                'isi_informasi' => $this->input->post('isi_informasi'),
                'image' => $upload['file_name'],
            );
        }
        
        $insert = $this->m_crud->insert('informasi',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function editinformasi()
    {
        $id = simple_decrypt($this->input->post('id_informasi'));
        $var = 'image';
        $dir = './assets/image/informasi/';
        $all = 'jpg|png|gif|jpeg|ico';
        $upload = $this->m_crud->uploadfile01($var,$dir,$all);
        
        if ( $upload == "")
        {
            $data = array(
                'judul_informasi' => $this->input->post('judul_informasi'),
                'isi_informasi' => $this->input->post('isi_informasi'),
            );
        }
        else
        {
            $data = array(
                'judul_informasi' => $this->input->post('judul_informasi'),
                'isi_informasi' => $this->input->post('isi_informasi'),
                'image' => $upload['file_name'],
            );
        }
        
        $insert = $this->m_crud->update('informasi','id_informasi',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function hapus()
    {
        $id = simple_decrypt($this->input->post('id'));
        
        $delete = $this->m_crud->delete('informasi','id_informasi',$id);

        if($delete == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
}
