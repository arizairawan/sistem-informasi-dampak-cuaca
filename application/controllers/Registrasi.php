<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	public function index()
	{
        $data = array(
            'kecamatan' => $this->m_global->get_all('kecamatan'),
            'kota' => $this->m_global->get_all('kota'),
        );
		$this->load->view('registrasi',$data);
	}
    
    public function proses()
    {
        $var = 'foto_ktp';
        $dir = './assets/ktp/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        $var = 'pas_foto';
        $dir = './assets/foto/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload2 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        if ( $upload1 == "" || $upload2 == "")
        {
            echo "error";
        }
        else
        {
            if($this->input->post('password1') == $this->input->post('password2'))
            {
                $data = array(
                    'nama_relawan' => $this->input->post('nama_relawan'),
                    'no_ktp' => $this->input->post('no_ktp'),
                    'alamat' => $this->input->post('alamat'),
                    'telepon' => $this->input->post('telepon'),
                    'id_kecamatan' => $this->input->post('id_kecamatan'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password1')),
                    'pas_foto' => $upload1['file_name'],
                    'foto_ktp' => $upload2['file_name'],
                );
                $this->m_crud->insert('relawan',$data);
                echo "success";
            }
            else
            {
                echo "password";
            }
            
        }
    }
}
