<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Printsurat extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE
        $this->load->library('pdfgenerator');
    }
    function sk($id)
    {
        $ids = simple_decrypt($id);

        $data['relawan'] = $this->m_global->get_by_id('relawan','id_relawan',$ids);
        $data['hasil'] = $this->m_global->get_by_id('hasil_pelatihan','id_relawan',$ids);
        $data['nomor'] = $ids;
        
        if($data['hasil']['status_validasi'] != 'Y')
        {
            echo"<center><h2>Data tidak valid!</h2></center>";
        }
        else
        {
            $html = $this->load->view('printsurat', $data, true);
            $filename = 'report_'.time();
            $this->pdfgenerator->generate($html, $filename, true, 'A4', 'portrait');
        }
        
    }
}