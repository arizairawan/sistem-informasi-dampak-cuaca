<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {
    public $title = 'Request';
    public function __construct() {
        parent::__construct();
        
    }
	public function index()
	{
        $data = array(
            'nav' => 'request',
            'title' => $this->title,
            'views' => 'request_table',
            'level' => $this->m_global->get_all('level'),
            'kota' => $this->m_global->get_all('kota'),
            'kecamatan' => $this->m_global->get_all('kecamatan'),
        );
		$this->load->view('starter',$data);
	}
    public function detail($id)
	{
        $request = $this->m_global->get_by_id('request','id_request',simple_decrypt($id));
        $oleh = $this->m_global->get_by_id('user','id_user',$request['id_user']);
        
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$request['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$request['id_kota']);
        
        $ver = $this->m_global->get_by_id('user','id_user',$request['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$request['id_user_validasi']);
        
        if($kec['nama_kecamatan'] == ''){
            $kecamatanname = 'Semua Kecamatan';
        }else{
            $kecamatanname = $kec['nama_kecamatan'];
        }
        
        $data = array(
            'nav' => 'request',
            'title' => $this->title,
            'views' => 'request_detail',
            'request' => $request,
            'oleh' => $oleh,
            'kota' => $kota['nama_kota'],
            'kecamatan' => $kecamatanname,
            'verifikasi' => cekstatus($request['status_verifikasi'],$request['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($request['status_validasi'],$request['tgl_validasi'], $val['nama_user']),
        );
		$this->load->view('starter',$data);
	}
    
    public function loaddata($id = false)
    {
        if($id === false)
        {
            $request = $this->m_global->get_all_order('request', 'id_request', 'desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $request = $this->m_global->get_list_by_id_order('request', 'id_user', $ids, 'id_request', 'desc');
        }
        

        $no = 1;
        $arr = array();
        foreach($request as $pry):
        
        
        $oleh = $this->m_global->get_by_id('user','id_user',$pry['id_user']);
        
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$pry['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$kec['id_kota']);
        
        $ver = $this->m_global->get_by_id('user','id_user',$pry['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$pry['id_user_validasi']);
        
        if($kec['nama_kecamatan'] == ''){
            $kecamatanname = 'Semua Kecamatan';
        }else{
            $kecamatanname = $kec['nama_kecamatan'];
        }
        
        $arra = array(
            'no' => $no++,
            'id_request' => simple_encrypt($pry['id_request']),
            'id_user' => $oleh['id_user'],
            'oleh' => $oleh['nama_user'],
            'tujuan_penggunaan' => $pry['tujuan_penggunaan'],
            'data' => $pry['data'],
            'id_kota' => $pry['id_kota'],
            'kota' => $kota['nama_kota'],
            'id_kecamatan' => $pry['id_kecamatan'],
            'kecamatan' => $kecamatanname,
            'tgl_awal' => $pry['tgl_awal'],
            'tgl_akhir' => $pry['tgl_akhir'],
            'tgl_request' => $pry['tgl_request'],
            'status_verifikasi' => $pry['status_verifikasi'],
            'tgl_verifikasi' => $pry['tgl_verifikasi'],
            'status_validasi' => $pry['status_validasi'],
            'tgl_validasi' => $pry['tgl_validasi'],
            'verifikasi' => cekstatus($pry['status_verifikasi'],$pry['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($pry['status_validasi'],$pry['tgl_validasi'], $val['nama_user']),
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function loaddatabyid($id = false)
    {
        $ids = simple_decrypt($id);
        $pry = $this->m_global->get_by_id('request', 'id_request', $ids);

        $oleh = $this->m_global->get_by_id('user','id_user',$pry['id_user']);
        
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$pry['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$kec['id_kota']);
        
        $ver = $this->m_global->get_by_id('user','id_user',$pry['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$pry['id_user_validasi']);
        
        $arra = array(
            'id_request' => simple_encrypt($pry['id_request']),
            'id_user' => $oleh['id_user'],
            'oleh' => $oleh['nama_user'],
            'tujuan_penggunaan' => $pry['tujuan_penggunaan'],
            'data' => $pry['data'],
            'id_kota' => $pry['id_kota'],
            'kota' => $kota['nama_kota'],
            'id_kecamatan' => $pry['id_kecamatan'],
            'kecamatan' => $kec['nama_kecamatan'],
            'tgl_awal' => $pry['tgl_awal'],
            'tgl_akhir' => $pry['tgl_akhir'],
            'tgl_request' => $pry['tgl_request'],
            'status_verifikasi' => $pry['status_verifikasi'],
            'tgl_verifikasi' => $pry['tgl_verifikasi'],
            'status_validasi' => $pry['status_validasi'],
            'tgl_validasi' => $pry['tgl_validasi'],
            'verifikasi' => cekstatus($pry['status_verifikasi'],$pry['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($pry['status_validasi'],$pry['tgl_validasi'], $val['nama_user']),
        );


        
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arra, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function addrequest()
    {
        $var = 'surat_permohonan';
        $dir = './assets/lampiran/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload = $this->m_crud->uploadfile01($var,$dir,$all);
        
        if($upload != ''){
            $data = array(
                'data' => 'curahhujan',
                'id_kota' => $this->input->post('id_kota'),
                'id_kecamatan' => $this->input->post('id_kecamatan'),
                'tgl_awal' => $this->input->post('tgl_awal'),
                'tgl_akhir' => $this->input->post('tgl_akhir'),
                'tgl_akhir' => $this->input->post('tgl_akhir'),
                'tujuan_penggunaan' => $this->input->post('tujuan_penggunaan'),
                'surat_permohonan' => $upload['file_name'],
                'id_user' => $_SESSION['id_user']
            );
            $insert = $this->m_crud->insert('request',$data);
            echo"success";
        }else{
            echo"gagal";
        }
        
        
    }
    
    public function editrequest()
    {
        $var = 'surat_permohonan';
        $dir = './assets/lampiran/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload = $this->m_crud->uploadfile01($var,$dir,$all);
        
        $id = simple_decrypt($this->input->post('id_request'));
        $data = array(
            'data' => 'curahhujan',
            'id_kota' => $this->input->post('id_kota'),
            'id_kecamatan' => $this->input->post('id_kecamatan'),
            'tgl_awal' => $this->input->post('tgl_awal'),
            'tgl_akhir' => $this->input->post('tgl_akhir'),
            'tgl_akhir' => $this->input->post('tgl_akhir'),
            'tujuan_penggunaan' => $this->input->post('tujuan_penggunaan'),
        );
        if($upload != ''){
            $data['surat_permohonan'] = $upload['file_name'];
        }
        $insert = $this->m_crud->update('request','id_request',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function hapus()
    {
        $id = simple_decrypt($this->input->post('id'));
        
        $delete = $this->m_crud->delete('request','id_request',$id);

        if($delete == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function verifikasi()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_verifikasi' => 'Y',
            'tgl_verifikasi' => date('Y-m-d'),
            'id_user_verifikasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('request','id_request',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    
    public function validasi()
    {
        $id = simple_decrypt($this->input->post('id'));
        $data = array(
            'status_validasi' => 'Y',
            'tgl_validasi' => date('Y-m-d'),
            'id_user_validasi' => $_SESSION['id_user'],
        );
        $insert = $this->m_crud->update('request','id_request',$data, $id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    
    public function download($id)
    {
        $this->load->library('pdfgenerator');
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE
        
        $ids = simple_decrypt($id);
        $request = $this->m_global->get_by_id('request', 'id_request', $ids);
        
        $tgl_awal = $request['tgl_awal'];
        $tgl_akhir = $request['tgl_akhir'];
        $idkec = $request['id_kecamatan'];
        $idkota = $request['id_kota'];
        
        $data['kecamatan'] = $this->m_global->get_by_id('kecamatan', 'id_kecamatan', $idkec);
        $kota = $this->m_global->get_by_id('kota', 'id_kota', $idkota);
        
        
        $data['request'] = $request;
        
        
        if($data['kecamatan']['nama_kecamatan'] == ''){
            $data['getdata'] = $this->db->query("select *, avg(curahhujan) as avgch from data_curahhujan left join relawan on relawan.id_relawan = data_curahhujan.id_relawan left join kecamatan on kecamatan.id_kecamatan = relawan.id_kecamatan left join kota on kota.id_kota = kecamatan.id_kota where kota.id_kota = '$idkota' and tgl_lapor between '$tgl_awal' and '$tgl_akhir' group by tgl_lapor order by tgl_lapor asc")->result_array();
            
            $data['daerah'] = 'Kota '.$kota['nama_kota'];
            
        }else{
            $data['getdata'] = $this->db->query("select *, avg(curahhujan) as avgch from data_curahhujan left join relawan on relawan.id_relawan = data_curahhujan.id_relawan where id_kecamatan = '$idkec' and tgl_lapor between '$tgl_awal' and '$tgl_akhir' group by tgl_lapor order by tgl_lapor asc")->result_array();
            
            $data['daerah'] = 'Kecamatan '.$data['kecamatan']['nama_kecamatan'].' '.$kota['nama_kota'];
        }
        

        $html = $this->load->view('table_report', $data, true);
        $filename = 'report_'.time();
        $this->pdfgenerator->generate($html, $filename, true, 'A4', 'portrait');

    }
}
