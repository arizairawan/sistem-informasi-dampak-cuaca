<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}
    
    function hitung($id, $status, $tgls)
    {
        $tgl = date('Y-m-d');
        $bln = date('Y-m');
        
        $d = date('d');
        
        if($status == 'N' && $tgls == '0000-00-00')
        {
            $getlaporan = $this->db->query("SELECT * FROM `data_curahhujan` WHERE date_format(tgl_lapor, '%Y-%m') = '$bln' and id_relawan = '$id' and tgl_lapor <= '$tgl'")->num_rows();
        }
        else if($status == 'N' && $tgls != '0000-00-00')
        {
            $getlaporan = $this->db->query("SELECT * FROM `data_curahhujan` WHERE date_format(tgl_lapor, '%Y-%m') = '$bln' and id_relawan = '$id' and tgl_lapor >= '$tgls'")->num_rows();
            if(date('m') == date('m', strtotime($tgls))){
                $d = $d - date('d', strtotime($tgls));
            }
        }
        else
        {
            $getlaporan = 0;
        }
        
        return $d - $getlaporan;
    }
    
    function relawan()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        
        $login = $this->m_global->login_cek('email', $email, md5($password), 'relawan');
        if ($login == 1) 
        {
            $row = $this->m_global->data_login('email', $email, md5($password), 'relawan');
            
            $ceksuspend = $this->hitung($row['id_relawan'], $row['suspend_relawan'], $row['tgl_suspend']);
            
            if($ceksuspend >= 3)
            {
                if($row['suspend_relawan'] == 'N')
                {
                    $suspend = array(
                        'suspend_relawan' => 'Y',
                        'tgl_suspend' => date('Y-m-d')
                    );
                    $this->m_crud->update('relawan', 'id_relawan', $suspend, $row['id_relawan']);
                }
                
                echo"suspend";
            }
            else
            {
                $data = array(
                    'logged' => TRUE,
                    'tipe' => 'relawan',
                    'level' => '',
                    'id_user' => $row['id_relawan'],
                    'nama' => $row['nama_relawan'],
                );
                $this->session->set_userdata($data);
                echo"success";
            }
            
        } 
        else 
        {
            $login = $this->m_global->login_cek('email', $email, md5($password), 'stasiun');
            if ($login == 1) 
            {
                $row = $this->m_global->data_login('email', $email, md5($password), 'stasiun');
                $data = array(
                    'logged' => TRUE,
                    'tipe' => 'stasiun',
                    'level' => '',
                    'id_user' => $row['id_stasiun'],
                    'nama' => $row['nama_stasiun'],
                );
                $this->session->set_userdata($data);
                echo"success";
            }
            else
            {
                echo"error";   
            }
        }
    }
    
    function internal()
    {
        $email = $this->input->post('username');
        $password = $this->input->post('password');

        $login = $this->m_global->login_cek('username', $email, md5($password), 'user');
        if ($login == 1) 
        {
            $row = $this->m_global->data_login('username', $email, md5($password), 'user');
            $data = array(
                'logged' => TRUE,
                'tipe' => 'internal',
                'level' => $row['level'],
                'id_user' => $row['id_user'],
                'nama' => $row['nama_user'],
            );
            $this->session->set_userdata($data);
            echo"success";
        } 
        else 
        {
            echo"error";
        }
    }
    
    function out() 
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}
