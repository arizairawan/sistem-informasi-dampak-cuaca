<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporstasiun extends CI_Controller {
    public $title = 'Lapor';
    public function __construct() {
        parent::__construct();
//        if ($this->session->userdata('logged')<>1) {
//            redirect(site_url('login'));
//        }
    }
	public function index()
	{
        $data = array(
            'nav' => 'laporstasiun',
            'title' => $this->title." Curah hujan",
            'views' => 'lapor_table2',
        );
		$this->load->view('starter',$data);
	}
    public function suhu()
	{
        $data = array(
            'nav' => 'laporstasiunsuhu',
            'title' => $this->title." Suhu",
            'views' => 'lapor_table3',
        );
		$this->load->view('starter',$data);
	}
    public function loaddata($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('data_curahhujan','id_curahhujan','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id_order('data_curahhujan','id_stasiun',$ids, 'id_curahhujan', 'desc');
        }
        

        $no = 1;
        $arr = array();
        foreach($proyek as $pry):
        
        $relawan = $this->m_global->get_by_id('relawan','id_relawan',$pry['id_relawan']);
        $ver = $this->m_global->get_by_id('user','id_user',$pry['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$pry['id_user_validasi']);
        
        $arra = array(
            'no' => $no++,
            'id_curahhujan' => simple_encrypt($pry['id_curahhujan']),
            'curahhujan' => $pry['curahhujan'],
            'lampiran' => $pry['lampiran'],
            'tgl_lapor' => $pry['tgl_lapor'],
            'status_verifikasi' => $pry['status_verifikasi'],
            'tgl_verifikasi' => $pry['tgl_verifikasi'],
            'status_validasi' => $pry['status_validasi'],
            'tgl_validasi' => $pry['tgl_validasi'],
            'verifikasi' => cekstatus($pry['status_verifikasi'],$pry['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($pry['status_validasi'],$pry['tgl_validasi'], $val['nama_user']),
            'id_relawan' => simple_encrypt($pry['id_relawan']),
            'nama_relawan' => $relawan['nama_relawan']
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );
        
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function addcurahhujan()
    {
        $var = 'lampiran';
        $dir = './assets/lampiran/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);

        $data = array(
            'curahhujan' => $this->input->post('curahhujan'),
            'lampiran' => $upload1['file_name'],
            'id_stasiun' => $_SESSION['id_user']
        );
        $insert = $this->m_crud->insert('data_curahhujan',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    public function editcurahhujan()
    {
        $var = 'lampiran';
        $dir = './assets/lampiran/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);

        $id = simple_decrypt($this->input->post('id_curahhujan'));
        $data = array(
            'curahhujan' => $this->input->post('curahhujan'),
        );
        
        if($upload1['file_name'] != ''){
            $data['lampiran'] = $upload1['file_name'];    
        }
        
        $insert = $this->m_crud->update('data_curahhujan','id_curahhujan',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function loaddatakelembapan($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('data_kelembapan','id_kelembapan','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id('data_kelembapan','id_stasiun',$ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($proyek as $pry):
        
        $stasiun = $this->m_global->get_by_id('stasiun','id_stasiun',$pry['id_stasiun']);
        
        $ver = $this->m_global->get_by_id('user','id_user',$pry['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$pry['id_user_validasi']);
        
        $arra = array(
            'no' => $no++,
            'id_kelembapan' => simple_encrypt($pry['id_kelembapan']),
            'kelembapan' => $pry['kelembapan'],
            'lampiran' => $pry['lampiran'],
            'tgl_lapor' => $pry['tgl_lapor'],
            'status_verifikasi' => $pry['status_verifikasi'],
            'tgl_verifikasi' => $pry['tgl_verifikasi'],
            'status_validasi' => $pry['status_validasi'],
            'tgl_validasi' => $pry['tgl_validasi'],
            'verifikasi' => cekstatus($pry['status_verifikasi'],$pry['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($pry['status_validasi'],$pry['tgl_validasi'], $val['nama_user']),
            'id_stasiun' => simple_encrypt($pry['id_stasiun']),
            'nama_stasiun' => $stasiun['nama_stasiun']
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );
        
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function loaddatasuhu($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('data_suhu','id_suhu','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id('data_suhu','id_stasiun',$ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($proyek as $pry):
        
        $stasiun = $this->m_global->get_by_id('stasiun','id_stasiun',$pry['id_stasiun']);
        
        $ver = $this->m_global->get_by_id('user','id_user',$pry['id_user_verifikasi']);
        $val = $this->m_global->get_by_id('user','id_user',$pry['id_user_validasi']);
        
        $arra = array(
            'no' => $no++,
            'id_suhu' => simple_encrypt($pry['id_suhu']),
            'suhu_min' => $pry['suhu_min'],
            'suhu_max' => $pry['suhu_max'],
            'suhu_avg' => $pry['suhu_avg'],
            'lampiran' => $pry['lampiran'],
            'tgl_lapor' => $pry['tgl_lapor'],
            'status_verifikasi' => $pry['status_verifikasi'],
            'tgl_verifikasi' => $pry['tgl_verifikasi'],
            'status_validasi' => $pry['status_validasi'],
            'tgl_validasi' => $pry['tgl_validasi'],
            'verifikasi' => cekstatus($pry['status_verifikasi'],$pry['tgl_verifikasi'], $ver['nama_user']),
            'validasi' => cekstatus($pry['status_validasi'],$pry['tgl_validasi'], $val['nama_user']),
            'id_stasiun' => simple_encrypt($pry['id_stasiun']),
            'nama_stasiun' => $stasiun['nama_stasiun']
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );
        
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function addkelembapan()
    {
        $var = 'lampiran';
        $dir = './assets/lampiran/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);

        $data = array(
            'kelembapan' => $this->input->post('kelembapan'),
            'lampiran' => $upload1['file_name'],
            'id_stasiun' => $_SESSION['id_user']
        );
        $insert = $this->m_crud->insert('data_kelembapan',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    
    public function editkelembapan()
    {
        $var = 'lampiran';
        $dir = './assets/lampiran/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);

        $id = simple_decrypt($this->input->post('id_kelembapan'));
        $data = array(
            'kelembapan' => $this->input->post('kelembapan'),
        );
        
        if($upload1['file_name'] != ''){
            $data['lampiran'] = $upload1['file_name'];    
        }
        
        $insert = $this->m_crud->update('data_kelembapan','id_kelembapan',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function addsuhu()
    {
        $var = 'lampiran';
        $dir = './assets/lampiran/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);

        $data = array(
            'suhu_min' => $this->input->post('suhu_min'),
            'suhu_max' => $this->input->post('suhu_max'),
            'suhu_avg' => $this->input->post('suhu_avg'),
            'lampiran' => $upload1['file_name'],
            'id_stasiun' => $_SESSION['id_user']
        );
        $insert = $this->m_crud->insert('data_suhu',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function editsuhu()
    {
        $var = 'lampiran';
        $dir = './assets/lampiran/';
        $all = 'docx|docx|pdf|jpg|png|gif|jpeg|ico';
        $upload1 = $this->m_crud->uploadfile01($var,$dir,$all);
        
        
        $id = simple_decrypt($this->input->post('id_suhu'));
        
        $data = array(
            'suhu_min' => $this->input->post('suhu_min'),
            'suhu_max' => $this->input->post('suhu_max'),
            'suhu_avg' => $this->input->post('suhu_avg'),
        );
        
        if($upload1['file_name'] != ''){
            $data['lampiran'] = $upload1['file_name'];    
        }
        
        $insert = $this->m_crud->update('data_suhu','id_suhu',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function hapus()
    {
        $id = simple_decrypt($this->input->post('id'));
        
        $delete = $this->m_crud->delete('lapor','id_lapor',$id);

        if($delete == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
}
