<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends CI_Controller {
    public $title = 'Kota';
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
    }
	public function index()
	{
        $data = array(
            'nav' => 'kota',
            'title' => $this->title,
            'views' => 'kota_table'
        );
		$this->load->view('starter',$data);
	}
    public function loaddata($id = false)
    {
        if($id === false)
        {
            $proyek = $this->m_global->get_all_order('kota','id_kota','desc');
        }
        else
        {
            $ids = simple_decrypt($id);
            $proyek = $this->m_global->get_list_by_id('kota','id_kota',$ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($proyek as $pry):

        $arra = array(
            'no' => $no++,
            'id_kota' => simple_encrypt($pry['id_kota']),
            'nama_kota' => $pry['nama_kota'],
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function addkota()
    {
        $data = array(
            'nama_kota' => $this->input->post('nama_kota'),
        );
        $insert = $this->m_crud->insert('kota',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function editkota()
    {
        $id = simple_decrypt($this->input->post('id_kota'));
        $data = array(
            'nama_kota' => $this->input->post('nama_kota'),
        );
        $insert = $this->m_crud->update('kota','id_kota',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function hapus()
    {
        $id = simple_decrypt($this->input->post('id'));
        
        $delete = $this->m_crud->delete('kota','id_kota',$id);

        if($delete == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
}
