<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    public $title = 'User';
    public function __construct() {
        parent::__construct();
        
    }
	public function index()
	{
        $data = array(
            'nav' => 'user',
            'title' => $this->title,
            'views' => 'user_table',
            'level' => $this->m_global->get_all('level')
        );
		$this->load->view('starter',$data);
	}
    public function loaddata($id = false)
    {
        if($id === false)
        {
            $user = $this->m_global->get_all('user');
        }
        else
        {
            $ids = simple_decrypt($id);
            $user = $this->m_global->get_list_by_id('user', 'id_user', $ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($user as $pry):
        $level = $this->m_global->get_by_id('level', 'id_level', $pry['level']);
        $arra = array(
            'no' => $no++,
            'id_user' => simple_encrypt($pry['id_user']),
            'nama_user' => $pry['nama_user'],
            'username' => $pry['username'],
            'id_level' => $pry['level'],
            'level' => $level['level'],
            'tgl_input' => $pry['tgl_input'],
            'tgl_update' => $pry['tgl_update'],
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function adduser()
    {
        $data = array(
            'nama_user' => $this->input->post('nama_user'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'level' => $this->input->post('level'),
        );
        $insert = $this->m_crud->insert('user',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function edituser()
    {
        $id = simple_decrypt($this->input->post('id_user'));
        $data = array(
            'nama_user' => $this->input->post('nama_user'),
            'username' => $this->input->post('username'),
            'level' => $this->input->post('level'),
        );
        if($this->input->post('password') != ''){
            $data['password'] = md5($this->input->post('password'));
        }
        $insert = $this->m_crud->update('user','id_user',$data,$id);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    
    public function hapus()
    {
        $id = simple_decrypt($this->input->post('id'));
        
        $delete = $this->m_crud->delete('user','id_user',$id);

        if($delete == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
}
