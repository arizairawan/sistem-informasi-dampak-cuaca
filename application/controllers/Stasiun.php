<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stasiun extends CI_Controller {
    public $title = 'Stasiun';
    public function __construct() {
        parent::__construct();
        
    }
	public function index()
	{
        if ($this->session->userdata('logged')<>1) {
            redirect(site_url('login'));
        }
        $data = array(
            'nav' => 'stasiun',
            'title' => $this->title,
            'views' => 'stasiun_table',
            'kecamatan' => $this->m_global->get_all('kecamatan'),
            'kota' => $this->m_global->get_all('kota'),
        );
		$this->load->view('starter',$data);
	}
    
    function hitung($id)
    {
        
        $tgl = date('Y-m-d');
        $bln = date('Y-m');
//        $d=cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        
        $d = date('d');
        
        $getlaporan = $this->db->query("SELECT * FROM `data_curahhujan` WHERE date_format(tgl_lapor, '%Y-%m') = '$bln' and id_stasiun = '$id' and tgl_lapor <= '$tgl'")->num_rows();
        
        $data = array(
            'jml_hari' => $d,
            'jml_lapor' => $getlaporan,
            'jml_bolos' => $d - $getlaporan
        );
        return $data;
    }
    public function loaddata($id = false)
    {   
        if($id === false)
        {
            $stasiun = $this->m_global->get_all('stasiun');
        }
        else
        {
            $ids = simple_decrypt($id);
            $stasiun = $this->m_global->get_list_by_id('stasiun', 'id_stasiun', $ids);
        }
        

        $no = 1;
        $arr = array();
        foreach($stasiun as $pry):
        
        
        //kecamatan
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan', $pry['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota', $kec['id_kota']);
        
        
        $arra = array(
            'no' => $no++,
            'id_stasiun' => simple_encrypt($pry['id_stasiun']),
            'nama_stasiun' => $pry['nama_stasiun'],
            'alamat' => $pry['alamat'],
            'telepon' => $pry['telepon'],
            'email' => $pry['email'],
            'id_kecamatan' => $pry['id_kecamatan'],
            'nama_kecamatan' => $kec['nama_kecamatan'],
            'id_kota' => $kec['id_kota'],
            'nama_kota' => $kota['nama_kota'],
            'tgl_input' => $pry['tgl_input'],
            'jml' => $this->hitung($pry['id_stasiun'])
        );


        array_push($arr,$arra);
        endforeach;

        $arrayall = array(
            'data' => $arr,
            'total' => count($arr)
        );

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arrayall, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;
    }
    
    public function detail($id = false)
    {
        if($id === false)
        {
            show_404();
        }
        
        $ids = simple_decrypt($id);
        $stasiun = $this->m_global->get_by_id('stasiun','id_stasiun',$ids);
        $kec = $this->m_global->get_by_id('kecamatan','id_kecamatan',$stasiun['id_kecamatan']);
        $kota = $this->m_global->get_by_id('kota','id_kota',$kec['id_kota']);
        
        
        $data = array(
            'nav' => 'stasiun',
            'title' => $this->title." - ".$stasiun['nama_stasiun'],
            'views' => 'stasiun_detail',
            'stasiun' => $stasiun,
            'kec' => $kec,
            'kecamatan' => $this->m_global->get_all('kecamatan'),
            'kota' => $this->m_global->get_all('kota'),
        );
		$this->load->view('starter',$data);
    }
    
    public function prosesadd()
    {
        $data = array(
            'nama_stasiun' => $this->input->post('nama_stasiun'),
            'alamat' => $this->input->post('alamat'),
            'id_kecamatan' => $this->input->post('id_kecamatan'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'telepon' => $this->input->post('telepon'),
        );
        $insert = $this->m_crud->insert('stasiun',$data);
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
    public function prosesedit()
    {
        $data = array(
            'nama_stasiun' => $this->input->post('nama_stasiun'),
            'alamat' => $this->input->post('alamat'),
            'id_kecamatan' => $this->input->post('id_kecamatan'),
            'email' => $this->input->post('email'),
            'telepon' => $this->input->post('telepon'),
        );
        
        if($this->input->post('password') != ''){
            $data['password'] = md5($this->input->post('password'));
        }
           
        $insert = $this->m_crud->update('stasiun','id_stasiun',$data, $this->input->post('id_stasiun'));
        if($insert == 1){
            echo"success";
        }else{
            echo"gagal";
        }
    }
}